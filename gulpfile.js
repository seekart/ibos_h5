var pkg = require('./package.json');

var minimist = require('minimist');

var gulp = require('gulp');

var concat = require('gulp-concat');
var header = require('gulp-header');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');
var processhtml = require('gulp-processhtml');
var copy = require('gulp-copy');
var templateCache = require('gulp-angular-templatecache');
var imagemin = require('gulp-imagemin');
var minifyInline = require('gulp-minify-inline-scripts');
var ngDocs = require('gulp-ngdocs');
var cache = require('gulp-cache');
var pngquant = require('imagemin-pngquant');
var browserSync = require('browser-sync');

var __sourceDir = './www';
var __distDir = './www/distlocal';
var __paths = {
  ionic:        '/lib/ionic-v1.0.0-beta.13',
  sass:         '/lib/ionic-v1.0.0-beta.13/scss/**/*.scss',
  root:         '',
  lib:          '/lib',

  
  ibosRoot:     '/ibos',
  ibosJs:       '/ibos/js',
  ibosCss:      '/ibos/css',
  ibosTemplate: '/ibos/templates',
  ibosComponent: '/ibos/components',
  ibosLib:      '/ibos/lib',
};

var paths = {};
var distPaths = {};
for(var i in __paths) {
  paths[i] = __sourceDir + __paths[i];
  distPaths[i] = __distDir + __paths[i];
}

var bannerTemplate = ['/**',
  ' * <%= pkg.name %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @author <%= pkg.author %>',
  ' */',
  ''].join('\n');

// ---------------------- IBOS 应用相关构建任务
// 合并业务 JS
gulp.task('js', function(done) {
  gulp.src([
    paths.ibosJs + '/**/!(app)*.js',
    paths.ibosJs + '/app.js'
  ])
  .pipe(sourcemaps.init())
  .pipe(ngAnnotate())
  .pipe(concat('all.js'))
  .pipe(uglify({
    preserveComments: 'some'
  }))
  .pipe(header(bannerTemplate, { pkg : pkg }))
  .pipe(sourcemaps.write('./', { sourceRoot: './' }))
  .pipe(gulp.dest(distPaths.ibosJs))
  .on('end', done);
});

// 合并 angular template 并注册到 $templateCache
gulp.task('template', function (done) {
  gulp.src(paths.ibosTemplate + '/**/*.html')
  .pipe(copy(distPaths.ibosTemplate, { prefix: 3 }))
  .pipe(templateCache({
    module: 'ibosApp.templates',
    standalone: true,
    transformUrl: function(url) {
      return url.slice(url.indexOf('templates'));
    }
  }))
  .pipe(gulp.dest(distPaths.ibosTemplate))
  .on('end', done);
});

gulp.task('css', function(done) {
  gulp.src(paths.ibosCss + '/**/*.css')
  .pipe(concat('style.css'))
  .pipe(minifyCss({
    keepSpecialComments: 0
  }))
  .pipe(gulp.dest(distPaths.ibosCss))
  .on('end', done);
});

gulp.task('component', function(done) {
  gulp.src(paths.ibosComponent + '/**/*', {
    base: paths.ibosComponent
  })
  .pipe(copy(distPaths.ibosComponent, { prefix: 3 }))

});

// 压缩图片
gulp.task('imagemin', function(done) {
  gulp.src(paths.ibosRoot + '/img/**/*', { base: paths.ibosRoot })
    .pipe(cache(imagemin({
        // optimizationLevel: 3,
        // progressive: true,
        // svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    })))
    .pipe(gulp.dest(distPaths.ibosRoot))
    .on('end', done);
});

gulp.task('index', function(done) {
  // var options = minimist(progress.argv.slice(2));

  gulp.src(paths.ibosRoot + '/index.html')
  .pipe(processhtml({
    data: pkg
  }))
  .pipe(minifyInline())
  .pipe(rename({ extname: '.php' }))
  .pipe(gulp.dest(distPaths.ibosRoot))
  .on('end', done);
});

gulp.task('lib', function(done) {
  gulp.src(
    [
      paths.lib + '/ionic-1.1.0/**/*',
      paths.lib + '/basket/**/*',
    ],
    {
      base: paths.lib
    }
  )
  .pipe(copy(distPaths.ibosLib, { prefix: 2 }))

  gulp.src([
    paths.lib + '/ionic-1.1.0/js/ionic.bundle.js',
    paths.lib + '/angular/angular-locale_zh-cn.js',
    paths.lib + '/angular-filter/angular-filter.js',
    paths.lib + '/ocLazyLoad.min.js'
  ])
  .pipe(concat('/lib.js'))
  .pipe(uglify({
    preserveComments: 'some'
  }))
  .pipe(gulp.dest(distPaths.ibosLib))
  .on('end', done);
});


gulp.task('ibos', ['js', 'template', 'component', 'css', 'index', 'lib']);
gulp.task('ibosi', ['ibos', 'imagemin']);
gulp.task('default', ['ibosi']);

gulp.task('server', function() {
    var files = [
    '**/*.*'
    // '**/*.css',
    // '**/*.js'
    ];
    browserSync.init(files,{
        server: {
            baseDir: "./www"
        }
    });
});
