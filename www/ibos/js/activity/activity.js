/**
 * @ngdoc overview
 * @name  activity
 * @description
 *   活动模块入口及配置
 */
angular.module('activity', ['ionic', 'activity.controllers', 'activity.services'])

.run(function(Apps) {
  Apps.install({
    name: 'activity',
    title: '活动',
    route: '#/activity',
    state: 'activity.index',
    icon: 'img/modicon/activity.png'
  })
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/activity', '/activity/index');

  $stateProvider
  .state('activity', {
    url: '/activity',
    abstract: true,
    template: '<ion-nav-view name="activity"></ion-nav-view>',
    resolve: UserProvider.resolve
  })

  // 全部活动
  .state('activity.index', {
    url: '/index',
    views: {
      'activity': {
        templateUrl: 'templates/activity/activity-index.html',
        controller: 'ActivityIndexCtrl'
      }
    }
  })

  // 我参加的
  .state('activity.join', {
    url: '/join',
    views: {
      'activity': {
        templateUrl: 'templates/activity/activity-index.html',
        controller: 'ActivityJoinCtrl'
      }
    }
  })

  // 活动创建页
  .state('activity.create', {
    url: '/create/:id?formdataid',
    cache: false,
    views: {
      'activity': {
        templateUrl: 'templates/activity/activity-create.html',
        controller: 'ActivityCreateCtrl'
      }
    }
  })

  // 活动详情页
  .state('activity.view', {
    url: '/view/:id',
    cache: false,
    views: {
      'activity': {
        templateUrl: 'templates/activity/activity-view.html',
        controller: 'ActivityViewCtrl'
      }
    }
  })

  //活动安排页
  .state('activity.info', {
    url: '/info/:id',
    cache: false,
    views: {
      'activity': {
        templateUrl: 'templates/activity/activity-info.html',
        controller: 'ActivityInfoCtrl'
      }
    }
  })

  //活动安排页
  .state('activity.arrange', {
    url: '/arrange/:id',
    views: {
      'activity': {
        templateUrl: 'templates/activity/activity-arrange.html',
        controller: 'ActivityArrangeCtrl'
      }
    }
  })

})