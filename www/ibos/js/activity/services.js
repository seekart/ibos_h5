﻿/**
 * @ngdoc overview
 * @name  activity.services
 * @description
 *  活动模板服务
 */
angular.module('activity.services', [])

/**
 * @ngdoc service
 * @name  activity.services.Activity
 * @description
 *   任务模块接口服务
 */
.factory('Activity', function($http, Utils, Settings) {
  // 模块根目录
  var getModuleUrl = function () {
    return Settings.hostUrl + '?r=activity';
  }

  var Activity = {
    /**
     * @ngdoc service
     * @name  activity.services.list
     * @param    {object}       params
     * @description
     *    获取活动列表
     *    - `params.type`  {string}  类型，all所有活动、me我参加的活动
     *    - `params.page`  {number=} 页码，默认值为1
     * 
     * @returns  {HttpPromise}  HttpPromise
     */
    list: function(params) {
      return $http.get(getModuleUrl() + '/activityapi/index', {
        params: params
      });
    },

    /**
     * @ngdoc service
     * @name  activity.services.view
     * @param    {object}    params
     * @description
     *    获取活动详情
     *    - `params.id`  {string}  活动 id
     *    
     * @returns  {HttpPromise}  HttpPromise
     */
    view: function(params) {
      return $http.get(getModuleUrl() + '/activityapi/detail', {
        params: params
      });
    },

    /**
     * @ngdoc service
     * @name  activity.services.formField
     * @param    {object}    params
     * @description
     *    获取报名表单字段
     *    - `params.id`  {string}  活动 id
     *    
     * @returns  {HttpPromise}  HttpPromise
     */
    formField: function(params) {
      return $http.get(getModuleUrl() + '/signupapi/create', {
        params: params
      });
    },

    /**
     * @ngdoc service
     * @name  activity.services.createSignUp
     * @param    {object}    params
     * @description
     *    保存报名数据
     *    - `params.moduleid`  {string}  活动id
     *    - `params.uid`       {string}  用户id
     *    - `params.name`      {string}  用户名
     *    - `params.phone`     {string}  手机
     *    - `params.data1`     {string}  扩展字段
     *    ...
     *    
     * @returns  {HttpPromise}  HttpPromise
     */
    createSignUp: function(params) {
      return Utils.postJsonData(getModuleUrl() + '/signupapi/store', params);
    },

    /**
     * @ngdoc service
     * @name  activity.services.updateSignUp
     * @param    {object}    params
     * @description
     *    更新报名数据
     *    - `params.moduleid`    {string}  活动id
     *    - `params.formdataid`  {string}  报名数据id
     *    - `params.name`        {string}  用户名
     *    - `params.phone`       {string}  手机
     *    - `params.data1`       {string}  扩展字段
     *    ...
     *    
     * @returns  {HttpPromise}  HttpPromise
     */
    updateSignUp: function(params) {
      return Utils.postJsonData(getModuleUrl() + '/signupapi/update', params);
    },

    /**
     * @ngdoc service
     * @name  activity.services.deleteSignUp
     * @param    {object}    params
     * @description
     *    删除报名数据
     *    - `params.moduleid`    {string}  活动id
     *    - `params.formdataid`  {string}  报名数据id
     *    
     * @returns  {HttpPromise}  HttpPromise
     */
    deleteSignUp: function(params) {
      return $http.post(getModuleUrl() + '/signupapi/del', params);
    },

    /**
     * @ngdoc service
     * @name  activity.services.signUpList
     * @param    {object}    params
     * @description
     *    获取所有报名数据
     *    - `params.id`  {string}  活动id
     *    
     * @returns  {HttpPromise}  HttpPromise
     */
    signUpList: function(params) {
      return $http.get(getModuleUrl() + '/signupapi/detail', {
        params: params
      });
    },

    /**
     * @ngdoc service
     * @name  activity.services.arrangeList
     * @param    {object}    params
     * @description
     *    获取我的活动安排
     *    - `params.id`  {string}  活动id
     *    
     * @returns  {HttpPromise}  HttpPromise
     */
    arrangeList: function(params) {
      return $http.get(getModuleUrl() + '/arrangementapi/detail', {
        params: params
      });
    }
  };

  return Activity;
})

.factory('ActivityList', function(Activity, Utils) {
  return function(defaultParams) {
    var that = this;
    this.loading = false;
    this.hasMore = true;
    this.page = 1;
    this.list = [];
    // 加载更多
    this.load = function() {
      var promise;

      this.loading = true;

      promise = Activity.list(angular.extend({}, defaultParams, {
        page: that.page
      }))
      .success(function(res) {
        if(res.isSuccess) {
          that.list = that.list.concat(res.data);
          that.hasMore = res.hasmore;
          that.page++;
          console.log(res)
        } else {
          Utils.promptNoPermission();
        }
      });

      promise.finally(function() {
        that.loading = false;
      });

      return promise;
    };
    // 重载
    this.reload = function() {
      this.page = 1;
      this.list.length = 0;
      this.hasMore = true;
      return this.load();
      // .finally(function() {
      //   $scope.$broadcast('scroll.refreshComplete');
      // });
    }
  }
})

/**
 * @ngdoc filter
 * @name activity.services.filter:phoneCrawler
 * @function
 * @description
 *  手机号码嗅探
 *  
 * @param   {string}  content  内容
 * @returns {string}  替换手机号码为可点击的内容
 */
.filter('phoneCrawler', function(REGEXP_ENUM) {
  var reg = /\b1\d{10}\b/g ; //使用/1\d{10}/g将图片名称解析成电话号码，导致无法正常解析图片；
  return function(content) {
    content = content || '';
    return content.replace(reg, function(phone) {
      return '<a href="tel:' + phone + '">' + phone + '</a>'
    });
  }
})