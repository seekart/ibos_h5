// Test Function
function getProvider(name){
  return angular.element(document.body).injector().get(name);
}

try {
  angular.module('ibosApp.templates');
} catch(e) {
  angular.module('ibosApp.templates', []);
}

var installed = [
  'calendar',
  'cabinet',
  'diary',
  'news',
  'docs',
  'contacts',
  'email',
  'workflow',
  'assignment',
  'thread',
  'activity',
  'vote',
  'crm',
  'report'
];

/**
 * @ngdoc module
 * @name ibosApp
 * @module ibosApp
 * @description
 *   Ibos app 入口模块
 */
angular.module('ibosApp', [
  'ionic',
  'oc.lazyLoad',
  'angular.filter',
  'datetime',
  'ibosApp.templates',
  'ibosApp.services',
  'ibosApp.filters',
  'ibosApp.directives',
  'ibosApp.controllers'
].concat(installed))

// @Debug: 调试用，将所有服务指向 window.sv 
.run(function($window, $injector) {
  $window.addEventListener('load', function() {
    var inj;
    function allServices(mod, r) {
      if (!r) {
        r = {};
        inj = angular.element(document.body).injector().get;
      }
      angular.forEach(angular.module(mod).requires, function(m) {allServices(m,r)});
      angular.forEach(angular.module(mod)._invokeQueue, function(a) {
        try { r[a[2][0]] = inj(a[2][0]); } catch (e) {}
      });
      return r;
    }

    $window.sv = allServices('ibosApp');
  }, false);
})

.run(function($rootScope, Utils) {
  // Set the document title when a new view is shown
  $rootScope.$on('$ionicView.afterEnter', function(ev, data) {
    if (data && data.title) {
      Utils.setDocumentTitle(data.title);
    }
  });
})

.config(function($httpProvider, $httpParamSerializerJQLikeProvider) {
  var $httpParamSerializer = $httpParamSerializerJQLikeProvider.$get();

  //Enable cross domain calls
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.withCredentials = true;

  // 此参数作为后端判断请求是否来自 cors 的依据
  $httpProvider.defaults.headers.common.ISCORS = true;

  // delete $httpProvider.defaults.headers.common['X-Requested-With'];
  // $httpProvider.defaults.transformResponse.push(function(data){})

  // 由于 $http 默认的数据格式为 application/json，但目前后端使用的是 form data
  // 所以此处做数据格式转换
  // Use x-www-form-urlencoded Content-Type
  // http://victorblog.com/2012/12/20/make-angularjs-http-service-behave-like-jquery-ajax/
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? $httpParamSerializer(data) : data;
  }];

  // 使用 TalkData 报错
  $httpProvider.interceptors.push(['$q', function($q) {
    return {
      response: function(response) {
        var res = response.data

        if(angular.isDefined(res.isSuccess) && !res.isSuccess) {
          if (typeof TDAPP !== 'undefined') {
            TDAPP.onEvent('response warn', 'api', {
              response: response              
            });
          }
        }

        return response
      },

      responseError: function(rejection) {
        if (typeof TDAPP !== 'undefined') {
          TDAPP.onEvent('response error', 'api', {
            response: rejection              
          });
        }
        return $q.reject(rejection)
      }
    }
  }])
})

.config(function($animateProvider) {
  // $animateProvider.classNameFilter(/angular-animate/);
  $animateProvider.classNameFilter(/^(?:(?!no-animation).)*$/);
})

.config(function($compileProvider) {
  // Default /^\s*((https?|ftp|file):|data:image\/)/
  // 添加 blob 协议进 angular 模板解析的白名单
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|blob):|data:image\/)/);
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  

  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })

    .state('portal', {
      url: '/portal',
      templateUrl: 'templates/tab-apps.html',
      controller: 'AppsCtrl',
      resolve: UserProvider.resolve
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/portal');
})

.constant('$ionicLoadingConfig', {
  noBackdrop: true,
  template: '<ion-spinner></ion-spinner>'
})

// ionic 框架一些基本配置
.config(function($ionicConfigProvider) {

  // $ionicConfigProvider.platform.ios.backButton.text('返回');

  $ionicConfigProvider.backButton.text('返回').icon('ion-ios-arrow-back').previousTitleText(true);
  $ionicConfigProvider.tabs.style('standard').position('bottom');
  $ionicConfigProvider.navBar.alignTitle('center').positionPrimaryButtons('left').positionSecondaryButtons('right');
  $ionicConfigProvider.views.transition('ios');

  // 加载中动画风格
  // android, ios, ios-small, bubbles, circles, crescent, dots, lines, ripple, or spiral.
  $ionicConfigProvider.spinner.icon('android');
})

.config(function($provide) {

  $provide.decorator('$ionicHistory', function ($delegate) {

    /**
     * @ngdoc method
     * @name $ionicHistory#removeBackView
     * @description Remove the previous view from the history completely, including the 
     * cached element and scope (if they exist).
     */
    $delegate.removeBackView = function(){
      var self = this;
      var viewHistory = self.viewHistory();
      var currentHistory = viewHistory.histories[this.currentHistoryId()];
      var currentCursor = currentHistory.cursor;

      var currentView = currentHistory.stack[currentCursor];
      var backView = currentHistory.stack[currentCursor - 1];
      var replacementView = currentHistory.stack[currentCursor - 2];

      // fail if we dont have enough views in the history
      if (!backView || !replacementView) {
        return;
      }

      // remove the old backView and the cached element/scope
      currentHistory.stack.splice(currentCursor - 1, 1);
      self.clearCache([backView.viewId]);
      // make the replacementView and currentView point to each other (bypass the old backView)
      currentView.backViewId = replacementView.viewId;
      currentView.index = currentView.index - 1;
      replacementView.forwardViewId = currentView.viewId;
      // update the cursor and set new backView
      viewHistory.backView = replacementView;
      currentHistory.currentCursor += -1;
    };

    return $delegate;
  })
})

.run(function($document, $rootScope, $ionicHistory, Settings){
  $rootScope.isWeiXin = /micromessenger/.test(window.navigator.userAgent.toLowerCase());

  $rootScope.hasBackView = function() {
    return !!$ionicHistory.backView();
  };

  $rootScope.isBackView = function(stateId) {
    var backView = $ionicHistory.backView();
    if(backView) {
      if(angular.isString(stateId)) {
        return backView.stateId == stateId;
      } else if(angular.isArray(stateId)) {
        return stateId.indexOf(backView.stateId) !== -1;
      }
    }
    return false;
  };
})

.run(function($rootScope, TalkingData) {

  $rootScope.$on('$stateChangeSuccess', function($event, toState, toStateParams, fromState) {
    if(toState.talkingDataLabel) {
      if(toState.talkingDataParams) {
        TalkingData.onEvent(toState.talkingDataLabel, '', toStateParams);
      } else {
        TalkingData.onEvent(toState.talkingDataLabel, '');
      }
    }
  });
})

angular.bootstrap(document.body, ['ibosApp']);