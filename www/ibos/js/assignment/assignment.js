/**
 * @ngdoc module
 * @name assignment
 * @description
 *   任务指派模块
 */

angular.module('assignment', ['ionic', 'assignment.controllers', 'assignment.services'])

.run(function(Apps) {
  Apps.install({ name: 'assignment', title: '任务指派', route: '#/assignment', state: 'assignment.index', icon: 'img/modicon/assignment.png' })
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/assignment', '/assignment/index');

  $stateProvider
  .state('assignment', {
    url: '/assignment',
    abstract: true,
    template: '<ion-nav-view name="assignment"></ion-nav-view>',
    resolve: UserProvider.resolve
  })

  // 任务指派-首页
  .state('assignment.index', {
    url: '/index',
    talkingDataLabel: '任务指派-首页',
    views: {
      'assignment': {
        templateUrl: 'templates/assignment/assignment-index.html',
        controller: 'AssignmentIndexCtrl'
      }
    }
  })

  // 任务指派-未完成列表
  .state('assignment.unfinished', {
    url: '/unfinished?uid',
    views: {
      'assignment': {
        templateUrl: 'templates/assignment/assignment-unfinished.html',
        controller: 'AssignmentUnfinishedCtrl'
      }
    }
  })

  // 任务指派-已完成列表
  .state('assignment.finished', {
    url: '/finished',
    views: {
      'assignment': {
        templateUrl: 'templates/assignment/assignment-finished.html',
        controller: 'AssignmentFinishedCtrl'
      }
    }
  })

  // 任务指派-新建任务
  .state('assignment.create', {
    url: '/create',
    talkingDataLabel: '任务指派-新建任务',
    views: {
      'assignment': {
        templateUrl: 'templates/assignment/assignment-create.html',
        controller: 'AssignmentCreateCtrl'
      }
    }
  })

  // 任务指派-编辑任务
  .state('assignment.edit', {
    url: '/edit/:id',
    talkingDataLabel: '任务指派-编辑任务',
    views: {
      'assignment': {
        templateUrl: 'templates/assignment/assignment-edit.html',
        controller: 'AssignmentEditCtrl'
      }
    }
  })

  // 任务指派-查看任务
  .state('assignment.detail', {
    url: '/detail/:id',
    talkingDataLabel: '任务指派-查看任务',
    views: {
      'assignment': {
        templateUrl: 'templates/assignment/assignment-detail.html',
        controller: 'AssignmentDetailCtrl'
      }
    }
  })
});