/**
 * @ngdoc module
 * @name cabinet
 * @description
 *   文件柜模块入口
 */

angular.module('cabinet', ['ionic', 'cabinet.controllers', 'cabinet.services', 'cabinet.filters'])

.run(function(Apps) {
  Apps.install({ name: 'cabinet', title: '文件柜', route: '#/cabinet', state: 'cabinet.personal', icon: 'img/modicon/cabinet.png' });
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/cabinet', '/cabinet/personal');

  $stateProvider
  // setup an abstract state for the tabs directive
  .state('cabinet', {
    url: "/cabinet",
    abstract: true,
    templateUrl: "templates/cabinet/cabinet.html",
    resolve: UserProvider.resolve
  })

  .state('cabinet.personal', {
    url: '/personal?pid',
    talkingDataLabel: '文件柜-我的网盘',
    views: {
      'cabinet-personal': {
        templateUrl: 'templates/cabinet/cabinet-personal.html',
        controller: 'CbPersionalCtrl'
      }
    }
  })

  .state('cabinet.company', {
    url: '/company?pid',
    talkingDataLabel: '文件柜-公司网盘',
    views: {
      'cabinet-company': {
        templateUrl: 'templates/cabinet/cabinet-company.html',
        controller: 'CbCompanyCtrl'
      }
    }
  })

  .state('cabinet.received', {
    url: '/received',
    talkingDataLabel: '文件柜-收到的共享',
    views: {
      'cabinet-received': {
        templateUrl: 'templates/cabinet/cabinet-received.html',
        controller: 'CbReceivedCtrl'
      }
    }
  })

  .state('cabinet.user', {
    url: '/user?fromuid&pid',
    views: {
      'cabinet-received': {
        templateUrl: 'templates/cabinet/cabinet-personal.html',
        controller: 'CbReceiveUserCtrl'
      }
    }
  })
});