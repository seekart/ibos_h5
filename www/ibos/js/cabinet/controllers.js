/**
 * @ngdoc module
 * @name cabinet.controllers
 * @description
 *   文件柜模块控制器
 */
angular.module('cabinet.controllers', [])

/**
 * @ngdoc controller
 * @name CbPersionalCtrl
 * @module cabinet.controllers
 * @description
 *   个人文件柜列表页控制器
 */
.controller('CbPersionalCtrl', function($scope, $stateParams, $ionicLoading, Settings, Cabinet, User, Utils){
    $ionicLoading.show();
    User.getAuthority().success(function(res){
      if(res.isSuccess){
        if(res.data.file){
          $scope.cb = new Cabinet(Settings.rootUrl + '/file');
          $scope.cb.fetch($stateParams)
            .success(function(res) {
              $scope.title = res.pDir.name;
            });
        } else {
          Utils.promptNoPermission();
        }
      }
    }).finally($ionicLoading.hide);

})

/**
 * @ngdoc controller
 * @name CbCompanyCtrl
 * @module cabinet.controllers
 * @description
 *   公司文件柜列表页控制器
 */
.controller('CbCompanyCtrl', function($scope, $state, $stateParams, Settings, Cabinet, ACCESS){
  $scope.cb = new Cabinet(Settings.rootUrl + '/companyfile');
  $scope.cb.fetch($stateParams)
  .success(function(res) {
    $scope.title = res.pDir.name;
  });

  $scope.cb.openFolder = function(file){
    $state.go('cabinet.company', { pid: file.fid });
  }

  var _showMenu = $scope.cb.showMenu;

  $scope.cb.showMenu = function(file){
    _showMenu.call($scope.cb, file, {
      remove: !file.access || file.access == ACCESS.WRITABLE
    });
  }
})


/**
 * @ngdoc controller
 * @name CbReceiveCtrl
 * @module cabinet.controllers
 * @description
 *   收到的共享，人员列表控制器
 */
.controller('CbReceivedCtrl', function($scope, CabinetReceived) {
  
  $scope.users = [];

  CabinetReceived.fetch()
  .success(function(res){
    $scope.users = res.data;
  });

  $scope.search = function(keyword) {
    CabinetReceived.fetch({
      keyword: keyword,
      normal_search: 1,
      search: 1
    })
    .success(function(res){
      $scope.users = res.data;
    })
  }
})


/**
 * @ngdoc controller
 * @name CbReceiveUserCtrl
 * @module cabinet.controllers
 * @description
 *   收到的共享，人员详细文件列表页控制器
 */
.controller('CbReceiveUserCtrl', function($scope, $state, $stateParams, Settings, Cabinet){
  $scope.cb = new Cabinet(Settings.hostUrl + '?r=file/fromshare');
  $scope.cb.fetch($stateParams)
  .success(function(res) {
    if(res.breadCrumbs.length === 1) {
      $scope.title = res.breadCrumbs[0].realname;
    } else if(res.breadCrumbs.length > 1){
      $scope.title = res.breadCrumbs[res.breadCrumbs.length - 1].name;
    }
  });

  $scope.cb.openFolder = function(file){
    $state.go('cabinet.user', { pid: file.fid });
  }

  var _showMenu = $scope.cb.showMenu;

  $scope.cb.showMenu = function(file){
    _showMenu.call($scope.cb, file, {
      remove: false
    });
  }
})

