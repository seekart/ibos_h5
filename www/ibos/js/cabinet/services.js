﻿/**
 * @ngdoc module
 * @name cabinet.services
 * @description
 *   文件柜模块相关服务
 */
angular.module('cabinet.services', [])

.constant('ACCESS', {
  WRITABLE: '2',
  READABLE: '1'
})

.constant('FILETYPE', {
  FOLDER: '1',
  FILE: '0'
})

/**
 * @ngdoc service
 * @name Cabinet
 * @module cabinet.services
 * @description
 *   文件模块数据交互及处理
 */
.factory('Cabinet', function($http, $state, $filter, $stateParams, $ionicLoading, $ionicPopup, $ionicActionSheet, fullUrlFilter, Settings, ImgViewer, Downloader, FILETYPE){

  function Cabinet(moduleUrl){
    this.moduleUrl = moduleUrl;
    this.pid = 0;
    this.files = [];
  }

  /**
   *
   * @ngdoc method
   * @name Cabinet#fetch
   * @description 
   *   获取文件列表数据
   *   返回数据格式为
   *   {
   *     breadCrumbs: Array  // 面包屑
   *     data: Array         // 文件列表
   *     pDir: Object        // 父目录
   *     page: Object        // 页码
   *     pid: 0              // 父目录 id
   *   }
   * @param {Obejct} param 查询条件，可能包括 pid | keyword...
   * @returns {Object} promise
   */
  Cabinet.prototype.fetch = function(param) {
    var _this = this;

    $ionicLoading.show();

    param = param || {};

    var httpPromise = $http.get(this.moduleUrl + '/getcate', {
        params: angular.extend({
          pid: this.pid,
        }, param)
      })
      .success(function(res){
        _this.files = res.data;
        _this.pid = param.pid || _this.pid;
      })
      .error(Settings.requestError)
      .success($ionicLoading.hide)
      .error($ionicLoading.hide)

    return httpPromise;
  };

  /**
   * @ngdoc method
   * @name Cabinet#search
   * @description 
   *   通过关键字搜索文件
   * 
   * @param {String} keyword  关键字
   * @returns {Object} promise
   */
  Cabinet.prototype.search = function(keyword) {
    return this.fetch(angular.extend({}, $stateParams, {
      keyword: keyword,
      normal_search: 1,
      search: 1
    }))
  }

  /**
   * @ngdoc method
   * @name Cabinet#open
   * @description 
   *   打开文件\文件夹，根据文件类型进入对应打开方法
   * 
   * @param {Object} file  文件对象
   * @returns
   */
  Cabinet.prototype.open = function(file) {
    if(file.type == FILETYPE.FOLDER) {
      this.openFolder(file)
    // thumb属性指向图像缩略图，目前可根据此属性判断是否图像
    // fileurl 指向文件源地址
    } else if(file.thumb) {
      this.openImage(file);
    } else {
      this.download(file);
      // FileViewer.view(file.filetype, $filter('attachUrl')(file.fileurl))
    }
  };

  /**
   * @ngdoc method
   * @name Cabinet#openFolder
   * @description 
   *   打开文件夹，实际行为是跳转到该文件夹对应路由
   * 
   * @param {Object} file  文件对象
   * @returns
   */
  Cabinet.prototype.openFolder = function(file) {
    $state.go('cabinet.personal', { pid: file.fid });
  };

  /**
   * @ngdoc method
   * @name Cabinet#openImage
   * @description 
   *   查看图片
   * 
   * @param {Object} file  文件对象
   * @returns
   */
  Cabinet.prototype.openImage = function(file) {
    var srcs = [];
    angular.forEach(this.files, function(f) {
      if(f.thumb) {
        srcs.push(fullUrlFilter(f.fileurl));
      }
    });

    ImgViewer.view(fullUrlFilter(file.fileurl), srcs);
  };

  /**
   * @ngdoc method
   * @name Cabinet#download
   * @description 
   *   下载文件\文件夹
   * 
   * @param {Object} file  文件对象
   * @returns
   */
  Cabinet.prototype.download = function(file) {
    Downloader.down(fullUrlFilter(file.entireattachurl || file.officereadurl));
  };

  /**
   * @ngdoc method
   * @name Cabinet#showMenu
   * @description 
   *   显示操作菜单
   * 
   * @param {Object} file  文件对象
   * @param {Object} opt  菜单项配置
   *   * 可配置项包括 remove(删除)、download(下载)、share(共享)
   *   * 目前共享功能不可用
   * @returns
   */
  Cabinet.prototype.showMenu = function(file, opt) {

    var _this = this;
    var buttons = [];
    var handlers = [];

    opt = angular.extend({
      remove: true,
      download: true,
      share: false // 暂时屏蔽共享功能
    }, opt)

    // 下载按钮
    if(opt.download) {
      buttons.push({ text: '下载' });
      handlers.push(function(file){
        _this.download(file);
      });
    }

    // 共享按钮
    if(opt.share) {
      buttons.push({ text: '共享' });
      handlers.push(function(file){
        _this.share(file);
      })
    }

    var conf = {
      titleText: '文件操作',
      buttons: buttons,
      buttonClicked: function(index) {
        handlers[index](file);
        return true;
      },
      cancelText: '取消'
    }

    // 删除按钮
    if(opt.remove) {
      conf.destructiveText = '删除';
      conf.destructiveButtonClicked = function(){
        _this.remove(file);
      }
    }

    this.hideMenu = $ionicActionSheet.show(conf);
  };

  function removeFile(file, files) {
    angular.forEach(files, function(f, i){
      if(file.fid == f.fid) {
        files.splice(i, 1);
        return false;
      }
    });
  }

  /**
   *
   * @ngdoc method
   * @name Cabinet#remove
   * @description 
   *   删除指定 文件或文件夹至回收站
   * @param {String} fid  文件标识符|索引
   * @returns {Object} promise
   */
  Cabinet.prototype.remove = function(file) {
    var _this = this;

    $ionicPopup.confirm({
      title: '提示',
      template: '确定删除该文件/文件夹吗？',
      okText: '确定',
      cancelText: '取消'
    })
    .then(function(res) {
      if(res) {
        return $http.get(_this.moduleUrl + '/del&op=recycle', {
          params: { fids: file.fid }
        })
        .error(Settings.requestError)
        .success(function(res) {
          if(res.isSuccess) {
            removeFile(file, _this.files);
            _this.hideMenu();
          }
        });
      }
    });
    
  };

  return Cabinet;
})

.factory('CabinetReceived', function($http, $ionicLoading, Settings){

  var url = Settings.hostUrl + '?r=file/fromshare/getcate';

  function fetch(params) {
    $ionicLoading.show();

    return $http.get(url, {
      params: params
    })
    .error(Settings.requestError)
    .success($ionicLoading.hide)
    .error($ionicLoading.hide)
  }

  return {
    fetch: fetch
  };
})
