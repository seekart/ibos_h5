/**
 * Calendar
 * 日程模块入口
 */
angular.module('calendar', ['ionic', 'calendar.controllers', 'calendar.services', 'calendar.filters'])

.run(function(Apps) {
  Apps.install({ name: 'calendar', title: '日程', route: '#/calendar', state: 'calendar', icon: 'img/modicon/calendar.png' });
})

.constant('DATE_FORMAT', 'yyyy-MM-dd')

.constant('TIME_FORMAT', 'HH:mm')

.constant('DAY_MILLISECOND', 86400000)

.config(function($stateProvider, UserProvider) {

  $stateProvider

    .state('calendar', {
      url: '/calendar',
      templateUrl: 'templates/calendar/calendar.html',
      talkingDataLabel: '日程-首页',
      controller: 'CalendarCtrl',
      resolve: UserProvider.resolve
    })

    .state('calendar-create', {
      url: '/create',
      templateUrl: 'templates/calendar/detail.html',
      talkingDataLabel: '日程-创建',
      controller: 'CalendarCreateCtrl',
      resolve: UserProvider.resolve
    })

    .state('calendar-detail', {
      url: '/calendar/:calendarId',
      templateUrl: 'templates/calendar/detail.html',
      talkingDataLabel: '日程-查看',
      controller: 'CalendarDetailCtrl',
      resolve: UserProvider.resolve
    });

});
