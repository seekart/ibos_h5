/**
 * Calendar
 * 日程模块过滤器
 */

angular.module('calendar.filters', [])

.filter('calendarTheme', function(){
  var themeSeries = ['#3497DB', '#A6C82F', '#F4C73B', '#EE8C0C', '#E76F6F', '#AD85CC', '#98B2D1', '#82939E']
  return function(category){
    // 由于在后台处理数据时，给予的默认值是 -1，对应的是主题数组中的第一个。
    category = (category && category) != '-1' ? category : 0;
    return themeSeries[category || 0];
  };
})

.filter('dayFormToday', function(){
	return function(day){
		switch(day) {
			case 0:
				return '今天';

			case 1:
				return '明天';

			case 2:
				return '后天';

			case -1:
				return '昨天';

			case -2:
				return '前天';

			default:
				return day > 0 ? day + '天后' : -day + '天前';
		}
	}
})

.filter('calendarDate', [
	'$filter',
	'DAY_MILLISECOND',
	'DATE_FORMAT',

	function($filter, DAY_MILLISECOND, DATE_FORMAT){
		/**
		 * @ngdoc method
		 * @name CalendarUtil#formatDate
		 * @description
		 *   使用 dateFilter 格式化日期为 yyyy-MM-dd 格式
		 *   
		 * @param {object|string|number} date  日期对象、合法的日期字符串、时间戳
		 * @param {number} dayOffset  与传入日期的天数差
		 * @returns {stirng}
		 */
		function formatDate(date, dayOffset){
		  date = date || new Date();

		  if(dayOffset) {
		    date = +date + dayOffset * DAY_MILLISECOND;
		  }

		  return dateFilter(date, DATE_FORMAT);
		}

		return formatDate;
	}
])

.filter('calendarTime', [
	'$filter',
	'DAY_MILLISECOND',
	'TIME_FORMAT',

	function($filter, DAY_MILLISECOND, TIME_FORMAT){
		/**
		 * @ngdoc method
		 * @name CalendarUtil#formatTime
		 * @description
		 *   使用 dateFilter 格式化日期为 HH:mm 格式
		 *   
		 * @param {object} date  日期对象
		 * @param {number} minuteOffset  与传入日期的天数差
		 * @returns {stirng}
		 */
		function formatTime(date, minuteOffset){
		  date = date || new Date();

		  if(minuteOffset) {
		    date = +date + minuteOffset * DAY_MILLISECOND / 1440;
		  }

		  return dateFilter(date, TIME_FORMAT);
		}
	}
])

