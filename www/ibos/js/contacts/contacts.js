/**
 * @ngdoc module
 * @name contacts
 * @description
 *   通讯录模块入口
 */

angular.module('contacts', ['ionic', 'contacts.controllers','contact.services'])

.run(function(Apps) {
  Apps.install({ name: 'contacts', title: '通讯录', route: '#/contacts', state: 'contacts.list', icon: 'img/modicon/contacts.png' });
})

.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.when('/contacts', '/contacts/list/')

  $stateProvider

  .state('contacts', {
    url: "/contacts",
    abstract: true,
    template: '<ion-nav-view name="contacts"></ion-nav-view>',
    resolve: {
      user: function(User) {
        if(!User.isLogin) return User.login();
      }
    }
  })

  .state('contacts.list', {
    url: '/list/:deptid',
    talkingDataLabel: '通讯录-首页',
    views: {
      'contacts': {
        templateUrl: 'templates/contacts/contacts.html',
        controller: 'ContactsCtrl',
      }
    }

  })

  .state('contacts.detail', {
    url: '/detail/:id',
    talkingDataLabel: '通讯录-查看页',
    views: {
      'contacts': {
        templateUrl: 'templates/contacts/contacts-detail.html',
        controller: 'ContactsDetailCtrl',        
      }
    }
  })
    .state('contacts.group', {
      url: '/group/:id',
      talkingDataLabel: '通讯录-分组查看页',
      views: {
        'contacts': {
          templateUrl: 'templates/contacts/contact-group-detail.html',
          controller: 'ContactsGroupDetailCtrl',
        }
      }
    })

});