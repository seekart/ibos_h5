/**
 * @ngdoc module
 * @name contacts.controllers
 * @description
 *   通讯录模块控制器
 */
angular.module('contacts.controllers', [])

/**
 * @ngdoc controller
 * @name ContactsCtrl
 * @module contacts.controllers
 * @description
 *   列表页控制器
 */
  .controller('ContactsCtrl', function($scope, $ionicLoading, $stateParams, $ionicHistory, $ionicScrollDelegate, $ionicModal, $state, $q, IbPopup, SearchModal, Contact){
    var $ionContent = angular.element(document.querySelector('ion-content'));
    var popupPromise;
    function getDeptList(params) {
      $ionicLoading.show();
      Contact.getDeptList(params).success(function(res) {
        if(res.isSuccess) {
          $scope.deptInfo = res.data;
          $scope.categories  = res.data.depts;
          $scope.currentCategory = $scope.deptInfo;
          var deptid = res.data.deptid;
          if(deptid == '0'){
            $scope.currentCategory.title = '通讯录';
          } else {
            $scope.categorys = $scope.deptInfo.crumb;
          }

        } else {
          if(popupPromise && popupPromise.$$state.status === 0) return;
          popupPromise = IbPopup.alert({ title: res.msg }).then(function(){
            $ionicHistory.goBack();
          });
        }
      })
        .finally($ionicLoading.hide)
    }

    function getUserList(params) {
      $ionicLoading.show();
      Contact.getUserList(params).success(function(res) {
        if(res.isSuccess) {
          $scope.users = res.data;
        } else {
          if(popupPromise && popupPromise.$$state.status === 0) return;
          popupPromise = IbPopup.alert({ title: res.msg });
        }
      })
        .finally($ionicLoading.hide)
    }

    var deptid = $stateParams.deptid || 0;

    getDeptList({ deptid: deptid });
    getUserList({ deptid: deptid });

    $scope.enterCategory = function(category) {
      $state.go('contacts.list', category);
    };

    //去到某一个分类
    $scope.toCategory = function(category){
      var page = category - ($scope.categorys.length );
      $ionicHistory.goBack(page);
    };

    $scope.search = new SearchModal({
      scope: $scope,
      searchFunc: function(params){
        return Contact.search({
          search: params.key,
          deptid: $scope.deptInfo.deptid
        });
      }
    });

    //滚动固定面包屑
    $scope.showsearchbar = true;

    function onScroll(evt) {
      var distance = evt.detail.scrollTop;
      $scope.$applyAsync(function() {
        $scope.showsearchbar = distance > 44 ? false : true;
      });
    }

    $scope.$on('$ionicView.enter', function() {
      $ionContent.on('scroll', onScroll);
    });
    $scope.$on('$ionicView.leave', function() {
      $ionContent.off('scroll', onScroll);
    });

    // 监听路径历史，自动将面包屑滚动到末端
    var breadcrumbScrollInstance = $ionicScrollDelegate.$getByHandle('contact-breadcrumb-scroll');
    $scope.$watchCollection('histories', function() {
      breadcrumbScrollInstance.scrollBottom(true);
    });

  })

  .controller('ContactsDetailCtrl', function($scope, $stateParams, $ionicLoading, $state, IbPopup, Contact){
    $ionicLoading.show();
    $scope.user = [];
    Contact.getUserInfo($stateParams.id).success(function(res) {
      res.isSuccess ? $scope.user = res.data : IbPopup.alert({ title: res.msg });
    })
      .finally($ionicLoading.hide)
  })

  .controller('ContactsGroupDetailCtrl', function($scope, $stateParams, $ionicLoading, IbPopup, Contact){
    var deptId = $stateParams.id;

    //deptId 当前部门 id，如果为公司，该值为 0；
    if(deptId !== '0'){
      $ionicLoading.show();
      $scope.dept = [];
      Contact.getDeptInfo(deptId).success(function(res) {
        res.isSuccess ? $scope.dept = res.data : IbPopup.alert({ title: res.msg });
      })
        .finally($ionicLoading.hide)
    } else {
      $ionicLoading.show();
      $scope.corp = [];
      Contact.getCorpInfo().success(function(res) {
        res.isSuccess ? $scope.corp = res.data : IbPopup.alert({ title: res.msg });
      })
        .finally($ionicLoading.hide);
    }

  });