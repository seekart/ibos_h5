/**
 * @ngdoc service
 * @name contact.services.Contact
 * @description
 *   通讯录通用服务
 */

angular.module('contact.services', [])

  .factory('Contact', function($http,$ionicLoading, Settings) {
    var getModuleUrl = function () {
      return Settings.hostUrl + '?r=contact/api';
    }

    return {
      /*
       * @ngdoc    method
       * @name     contact.services.Contact#getDeptList
       * @methodOf contact.services.Contact
       * @description
       *    获取部门列表
       *    `params.deptid`  {string}  部门 id
       * @param   {object}         params  查询参数
       * @returns {httppromise}    httppromise
       */
      getDeptList : function(params) {
        return $http.get(getModuleUrl() + '/deptlist', { params: params })
      },

      /**
       * @ngdoc    method
       * @name     contact.services.Contact#getUserList
       * @methodOf contact.services.Contact
       * @description
       *    获取用户列表
       *    `params.deptid`  {string}  部门 id
       * @param   {object}         params  查询参数
       * @returns {httppromise}    httppromise
       */
      getUserList : function(params) {
        return $http.get(getModuleUrl() + '/userlist', { params: params })
      },

      /**
       * @ngdoc    method
       * @name     contact.services.Contact#search
       * @methodOf contact.services.Contact
       * @description
       *    搜索用户，包含子部门
       *    `params.deptid`  {string}  部门 id
       * @param   {object}         params  查询参数
       * @returns {httppromise}    httppromise
       */
      search: function(params) {
        return $http.get(getModuleUrl() + '/search', { params: params })
      },

      /**
       * @ngdoc    method
       * @name     contact.services.Contact#getUserInfo
       * @methodOf contact.services.Contact
       * @description
       *    获取用户信息
       * @param   {String}         userid  用户ID
       * @returns {httppromise}    httppromise
       */
      getUserInfo : function(userid) {
        return $http.get(getModuleUrl() + '/user', { params: { userid : userid }})
      },

      /**
       * @ngdoc    method
       * @name     contact.services.Contact#getCorpInfo
       * @methodOf contact.services.Contact
       * @description
       *    获取公司信息
       * @returns {httppromise}    httppromise
       */
      getCorpInfo : function() {
        return $http.get(getModuleUrl() + '/corp')
      },

      /**
       * @ngdoc    method
       * @name     contact.services.Contact#getDeptInfo
       * @methodOf contact.services.Contact
       * @description
       *    获取部门信息
       * @param   {String}         deptid  部门ID
       * @returns {httppromise}    httppromise
       */
      getDeptInfo : function(deptid) {
        return $http.get(getModuleUrl() + '/dept',{ params: { deptid : deptid }})
      }
    }
  })

