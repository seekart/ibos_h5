/**
 * @ngdoc module
 * @name crm
 * @description
 *   CRM模块
 */

angular.module('crm', ['ionic', 'crm.controllers', 'crm.services'])

.run(function(Apps) {
  Apps.install({
    name: 'crm',
    title: 'CRM',
    route: '#/crm',
    state: 'crm.index',
    icon: 'img/modicon/crm.png'
  })
})

.config(function($stateProvider, $urlRouterProvider, UserProvider, ResolversProvider) {

  $urlRouterProvider.when('/crm', '/crm/index');

  $stateProvider
    .state('crm', {
      url: '/crm',
      abstract: true,
      template: '<ion-nav-view name="crm"></ion-nav-view>',
      resolve: UserProvider.resolve
    })

  // CRM-门户
  .state('crm.index', {
    url: '/index',
    talkingDataLabel: 'CRM-门户',
    views: {
      'crm': {
        templateUrl: 'templates/crm/index.html',
        controller: 'CRMIndexCtrl'
      }
    }
  })

  // CRM-联系人-首页
  .state('crm.contact', {
    url: '/contact',
    talkingDataLabel: 'CRM-联系人-首页',
    views: {
      'crm': {
        templateUrl: 'templates/crm/contact.html',
        controller: 'CRMContactCtrl'
      }
    },
    defaultBack: {
      title: 'CRM',
      state: 'crm.index'
    }
  })

  // CRM-添加联系人
  .state('crm.addContact', {
    url: '/addcontact',
    talkingDataLabel: 'CRM-联系人-添加',
    params: { client: null },
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/contact-form.html',
        controller: 'CRMAddContactCtrl'
      }
    },
    resolve: ResolversProvider.get(['qqmap']),
    defaultBack: {
      title: '联系人',
      state: 'crm.contact'
    }
  })

  // CRM-编辑联系人
  .state('crm.editContact', {
    url: '/editcontact/:contactid',
    talkingDataLabel: 'CRM-联系人-编辑',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/contact-form.html',
        controller: 'CRMEditContactCtrl'
      }
    },
    resolve: ResolversProvider.get(['qqmap']),
    defaultBack: {
      title: '联系人',
      state: 'crm.contact'
    }
  })

  // CRM-联系人详情页
  .state('crm.contactView', {
    url: '/contactview/:contactid',
    talkingDataLabel: 'CRM-联系人-详情',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/contact-view.html',
        controller: 'CRMContactViewCtrl'
      }
    }
  })

  // CRM-客户
  .state('crm.client', {
    url: '/client',
    talkingDataLabel: 'CRM-客户-主页',
    views: {
      'crm': {
        templateUrl: 'templates/crm/client.html',
        controller: 'CRMClientCtrl'
      }
    },
    defaultBack: {
      title: 'CRM',
      state: 'crm.index'
    }
  })

  // CRM-添加客户
  .state('crm.addClient', {
    url: '/addclient',
    talkingDataLabel: 'CRM-客户-添加',
    params: { contact: null },
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/client-form.html',
        controller: 'CRMAddClientCtrl'
      }
    },
    resolve: ResolversProvider.get(['qqmap']),
    defaultBack: {
      title: '客户',
      state: 'crm.client'
    }
  })

  // CRM-编辑客户
  .state('crm.editClient', {
    url: '/editclient/:cid',
    talkingDataLabel: 'CRM-客户-编辑',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/client-form.html',
        controller: 'CRMEditClientCtrl'
      }
    },
    resolve: ResolversProvider.get(['qqmap']),
    defaultBack: {
      title: '客户',
      state: 'crm.client'
    }
  })

  // CRM-查看客户
  .state('crm.clientView', {
    url: '/clientview/:cid',
    talkingDataLabel: 'CRM-客户-查看',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/client-view.html',
        controller: 'CRMClientViewCtrl'
      }
    }
  })

  // CRM-商机首页
  .state('crm.opportunity', {
    url: '/opportunity',
    params: { tag: null , status: null },
    talkingDataLabel: 'CRM-商机-首页',
    views: {
      'crm': {
        templateUrl: 'templates/crm/opportunity.html',
        controller: 'CRMOpportunityCtrl'
      }
    },
    defaultBack: {
      title: 'CRM',
      state: 'crm.index'
    }
  })

  // CRM-商机详情页
  .state('crm.opportunityView', {
    url: '/opportunityview/:oid',
    talkingDataLabel: 'CRM-商机-详情页',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/opportunity-view.html',
        controller: 'CRMOpportunityViewCtrl'
      }
    }
  })

  // CRM-添加商机
  .state('crm.addOpportunity', {
    url: '/addopportunity',
    talkingDataLabel: 'CRM-商机-添加页',
    params: { client: null },
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/opportunity-form.html',
        controller: 'CRMAddOpportunityCtrl'
      }
    },
    defaultBack: {
      title: '商机',
      state: 'crm.opportunity'
    }
  })

  // CRM-编辑商机
  .state('crm.editOpportunity', {
    url: '/editopportunity/:oid',
    talkingDataLabel: 'CRM-商机-编辑页',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/opportunity-form.html',
        controller: 'CRMEditOpportunityCtrl'
      }
    },
    defaultBack: {
      title: '商机',
      state: 'crm.opportunity'
    }
  })

  // CRM-事件首页
  .state('crm.event', {
    url: '/event',
    talkingDataLabel: 'CRM-事件首页',
    views: {
      'crm': {
        templateUrl: 'templates/crm/event.html',
        controller: 'CRMEventCtrl'
      }
    },
    defaultBack: {
      title: 'CRM',
      state: 'crm.index'
    }
  })

  // CRM-添加事件
  .state('crm.addEvent', {
    url: '/addevent',
    talkingDataLabel: 'CRM-事件-添加页',
    cache: false,
    params: { client: null, opportunity: null, contact: null },
    views: {
      'crm': {
        templateUrl: 'templates/crm/event-form.html',
        controller: 'CRMAddEventCtrl'
      }
    },
    defaultBack: {
      title: '事件',
      state: 'crm.event'
    }
  })

  // CRM-编辑事件
  .state('crm.editEvent', {
    url: '/editevent/:eventid',
    talkingDataLabel: 'CRM-事件-编辑页',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/event-form.html',
        controller: 'CRMEditEventCtrl'
      }
    },
    defaultBack: {
      title: '事件',
      state: 'crm.event'
    }
  })

  // CRM-事件详情
  .state('crm.eventView', {
    url: '/eventview/:eventid',
    talkingDataLabel: 'CRM-事件-详情页',
    cache: false,
    views: {
      'crm': {
        templateUrl: 'templates/crm/event-view.html',
        controller: 'CRMEventViewCtrl'
      }
    }
  })

});