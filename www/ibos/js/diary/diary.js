/**
 * @ngdoc module
 * @name diary
 * @description
 *   工作日志模块
 */

angular.module('diary', ['ionic', 'diary.controllers', 'diary.services', 'diary.filters'])

.run(function(Apps) {
  Apps.install({ name: 'diary', title: '工作日志', route: '#/diary', state: 'diary.index', icon: 'img/modicon/diary.png' });
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/diary', '/diary/index');


  $stateProvider
    // setup an abstract state for the tabs directive
    .state('diary', {
      url: "/diary",
      abstract: true,
      template: '<ion-nav-view name="diary"></ion-nav-view>',
      resolve: UserProvider.resolve
    })

    .state('diary.index', {
      url: '/index',
      talkingDataLabel: '工作日志-主页',
      views: {
        'diary': {
          templateUrl: 'templates/diary/diary-index.html',
          controller: 'DiaryIndexCtrl'
        }
      }
    })

    .state('diary.personal', {
      url: '/personal',
      talkingDataLabel: '工作日志-我的日志',
      views: {
        'diary': {
          templateUrl: 'templates/diary/diary-personal.html',
          controller: 'DiaryPersionalCtrl'
        }
      }
    })

    .state('diary.create', {
      url: '/create',
      talkingDataLabel: '工作日志-写日志',
      cache: false,
      views: {
        'diary': {
          templateUrl: 'templates/diary/diary-create.html',
          controller: 'DiaryCreateCtrl'
        }
      }
    })

    .state('diary.edit', {
      url: '/edit/:id',
      talkingDataLabel: '工作日志-编辑日志',
      cache: false,
      views: {
        'diary': {
          templateUrl: 'templates/diary/diary-edit.html',
          controller: 'DiaryEditCtrl'
        }
      }
    })

    .state('diary.detail', {
      url: '/detail/:id',
      talkingDataLabel: '工作日志-查看日志',
      cache: false,
      views: {
        'diary': {
          templateUrl: 'templates/diary/diary-detail.html',
          controller: 'DiaryDetailCtrl'
        }
      }
    })

    .state('diary.review', {
      url: '/review?date',
      talkingDataLabel: '工作日志-点评下属',
      views: {
        'diary': {
          templateUrl: 'templates/diary/diary-review.html',
          controller: 'DiaryReviewCtrl'
        }
      }
    })

    .state('diary.follow', {
      url: '/follow?date',
      talkingDataLabel: '工作日志-交流日志',
      views: {
        'diary': {
          templateUrl: 'templates/diary/diary-follow.html',
          controller: 'DiaryFollowCtrl'
        }
      }
    })

    .state('diary.underling',{
     url:'/underling/:id',
     talkingDataLabel:'工作日志-下属日志',
      views: {
        'diary':{
          templateUrl:'templates/diary/diary-underling.html',
          controller:'DiaryUnderingCtrl'
        }
      }
    })

});