/**
 * @ngdoc module
 * @name diary.filters
 * @description
 *   工作日志模块过滤器
 */

angular.module('diary.filters', [])

/**
 * @ngdoc filter
 * @module diary.filters
 * @name diaryPercentage
 * @description
 *   将日志计划进度转化为百分比值
 */
.filter('diaryPercentage', function() {
	return function(val) {
		return val * 10 + '%';
	}
})

/**
 * @ngdoc filter
 * @module diary.filters
 * @name numToHHmm
 * @description
 *   将数字转化为00:00格式的时间
 */
.filter('numToHHmm', function() {

	function fillZero(num) {
		return num >= 10 ? '' + num : '0' + num;
	}

	function formatTime(num) {
		var hour = Math.floor(+num),
			minute = Math.floor((+num % 1) * 60);
		return fillZero(hour) + ':' + fillZero(minute);
	}

	return formatTime;
})

/**
 * @ngdoc filter
 * @module diary.filters
 * @name diaryTimeRemind
 * @description
 *   将数字段转化为00:00格式的时间段
 */
.filter('diaryTimeRemind', function(numToHHmmFilter) {
	// input => '15.5,16'
	// output => '15:30-16:00'
	return function(timeremind) {
		if(timeremind === '') return;
			var timeArr = timeremind.split(','); // ['15.5', '16']
			return numToHHmmFilter(timeArr[0]) + '-' + numToHHmmFilter(timeArr[1]);
	};
})

