/**
 * @ngdoc overview
 * @name  ibosApp.directives
 * @description
 *   通用指令
 */
angular.module('ibosApp.directives', [])

.directive('copyright', function() {
  return {
    restrict: 'E',
    template: '<p>Copyright © 2009-2017 IBOS 深圳市博思协创网络科技有限公司</p>',
    replace: true
  }
})

/**
 * @ngdoc    directive
 * @name     ibosApp.directives.directive:hideTabs
 * @element  ANY
 * @function
 * @restrict A
 *
 * @description
 *  是否隐藏当前面板的 tab 栏
 */
.directive('hideTabs', function($rootScope, $timeout) {
  return {
    restrict: 'A',
    link: function($scope, $el) {
      $rootScope.hideTabs = true;

      // $scope.$on('$ionicView.beforeLeave', function() {
      $scope.$on('$destroy', function() {
        $rootScope.hideTabs = false;
      });
    }
  };
})

/**
 * @ngdoc    directive
 * @name     ibosApp.directives.directive:imgPreview
 * @element  ANY
 * @restrict A
 *
 * @description
 *   为编辑器内容里的图片添加预览功能
 *   img-preview 的值用以分组，同一组图片在微信图片查看器内
 *   可通过上下张查看
 *   img-preview-src 用于指定查看大图时的大图地址，没有时默认取 src 的值
 */
.directive('imgPreview', function(ImgViewer) {
    var map = Array.prototype.map;

    return {
      restrict: 'AC',
      priority: 1,
      compile: function(element, attr) {
        return function(scope, $elem, attrs) {
          var src, srcs;
          var tagName = $elem[0].tagName.toLowerCase();

          // 如果指令绑定在 img 节点，则处理单个节点
          // img-preivew
          if (tagName === 'img') {
            var groupName = attrs.imgPreview || '';

            $elem.on('click', function() {
              var src = this.getAttribute('img-preview-src') || this.src;
              var siblings = document.querySelectorAll('img[img-preview="' + groupName + '"]');

              var srcs = map.call(siblings, function(sibling) {
                return sibling.getAttribute('img-preview-src') || sibling.src;
              });

              ImgViewer.view(src, srcs);
            });
            // 否则，以该节点下找到的所有 img 为一组
          } else {
            element.on('click', function(evt) {
              if (evt.target.tagName.toUpperCase() === 'IMG') {
                var imgs = $elem[0].querySelectorAll('img');
                srcs = map.call(imgs, function(img) {
                  return img.src;
                });
                ImgViewer.view(evt.target.src, srcs);
              }
            });
          }
        }
      }
    }
  })
  /**
   * @ngdoc    directive
   * @name     ibosApp.directives.directive:letter
   * @element  ANY
   * @restrict A
   * @todo 需要完善，监控 $hasHeader 等变量状态，为节点加上相关的class
   *  以自适应不同视图下的高度
   * @description
   *   生成字母索引工具条
   * @param  {string}  letter 索引 id 前缀
   */
  .directive('letter', function($location, $document, $timeout, $ionicLoading, $ionicScrollDelegate, $ionicLoadingConfig) {
    var map = Array.prototype.map;

    function createTemplate() {
      return '#ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('').map(function(value, key) {
        return '<a flag="' + value + '">' + value + '</a>';
      }).join('');
    }

    return {
      restrict: 'A',

      template: createTemplate(),

      compile: function(element, attr) {
        element.addClass("letter-bar");

        return function($scope, $elem, attrs) {
          var prefix = attrs.letter;
          var elemTop;
          var pos = getPos();
          var timer;

          // 获取所有字母索引的位置
          function getPos() {
            return map.call($elem.find('a'), function(anchor) {
              return {
                top: anchor.offsetTop,
                bottom: anchor.offsetTop + anchor.clientHeight,
                flag: anchor.getAttribute('flag')
              }
            });
          }

          // 根据触摸点与 letterBar 的距离获取此时对应的字母索引
          function getFlag(offsetTop) {
            if (offsetTop < pos[0].top) {
              return pos[0].flag;
            }
            if (offsetTop > pos[pos.length - 1].bottom) {
              return pos[pos.length - 1].flag;
            }

            for (var i = 0; i < pos.length; i++) {
              if (pos[i].top <= offsetTop && pos[i].bottom >= offsetTop) {
                return pos[i].flag;
              }
            }
          }

          // 延时至每 50 ms 执行一次
          function _mousemove(evt) {
            $timeout.cancel(timer);

            ionic.off('touchmove', _mousemove, $document[0]);

            var top = (evt.clientY || evt.touches[0].clientY) - elemTop;

            scrollTo(getFlag(top));

            timer = $timeout(function() {
              ionic.on('touchmove', _mousemove, $document[0]);
            }, 50);

            evt.stopPropagation();
            evt.preventDefault();
          }

          function scrollTo(flag) {
            var hash;
            var elem;

            flag = flag || "";
            $ionicLoading.show({
              noBackdrop: true,
              template: '<div class="letter-text">' + flag + '</div>'
            });

            hash = prefix + '_' + flag;
            // 对应节点存在时且处于显示状态时，才触发定位，否则会引发 anchorScroll 错误
            elem = document.getElementById(hash);
            if (elem && elem.style.display !== 'none') {
              // $anchorScroll(hash);
              $location.replace().hash(hash);
              $ionicScrollDelegate.anchorScroll();
            }
          }

          var oldIonicLoadingConfig;

          function _mousedown(evt) {
            var flag = this.getAttribute('flag');
            oldIonicLoadingConfig = angular.extend({}, $ionicLoadingConfig);
            elemTop = $elem[0].getBoundingClientRect().top;

            $elem.addClass('active');

            pos = getPos();

            scrollTo(flag);

            ionic.on('touchmove', _mousemove, $document[0]);
            ionic.on('touchend', _mouseup, $document[0]);
            ionic.on('touchcancel', _mouseup, $document[0]);

            evt.stopPropagation();
          }

          function _mouseup() {
            $timeout.cancel(timer);
            $elem.removeClass('active');
            $ionicLoading.hide();
            // 由于 $ionicLoading.show 方法传入 template 属性会影响到 $ionicLoadingConfig
            // 所以在 hide 之后，要还原回之前的值
            $ionicLoadingConfig.template = oldIonicLoadingConfig.template;

            ionic.off('touchmove', _mousemove, $document[0]);
            ionic.off('touchend', _mousemove, $document[0]);
            ionic.off('touchcancel', _mousemove, $document[0]);
          }

          angular.forEach($elem.find('a'), function(elem, index) {
            ionic.on('touchstart', _mousedown, elem);
          });

          var parentScope = $scope;
          $scope.$watch(function() {
            return (parentScope.$hasHeader ? ' has-header' : '') +
              (parentScope.$hasSubheader ? ' has-subheader' : '') +
              (parentScope.$hasFooter ? ' has-footer' : '') +
              (parentScope.$hasSubfooter ? ' has-subfooter' : '')
          }, function(className, oldClassName) {
            element.removeClass(oldClassName);
            element.addClass(className);
          });
        }
      }
    };
  })

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:userSelector
 * @element   INPUT
 * @restrict  E
 * @requires  ngModel
 *
 * @description
 *   人员选择显示区
 * @param     {String}  selectType             选择的数据类型
 * @param     {String}  userInfoFilter         数据过滤，过滤出要在列表项显示的数据
 */
.directive('userSelector', function($filter, removeFilter, userInfoFilter, departmentInfoFilter, positionInfoFilter) {

  return {
    restrict: 'E',
    replace: true,
    require: 'ngModel',
    scope: true,
    // transclude: true,
    template: '<div>' +
      '<div class="user-selector">' +
      '<span ng-repeat="val in collection" ng-click="remove(val)">{{ getName(val) }}</span>' +
      '</div>' +
      '<input type="hidden">' +
      '</div>',

    compile: function(element, attr) {
      // 过继各种有效属性到内部的 input
      var input = element.find('input');

      angular.forEach({
        'name': attr.name,
        'ng-value': attr.ngValue,
        'ng-model': attr.ngModel,
        'ng-disabled': attr.ngDisabled,
        'ng-change': attr.ngChange,
        'required': attr.required
      }, function(value, name) {
        if (angular.isDefined(value)) {
          input.attr(name, value);
        }
      });

      // Link
      return function($scope, $elem, attrs, $ngModelCtrl) {
        var selectType = attrs.selectType || 'user';

        $scope.getName = function(val) {
          if (selectType == 'user') {
            return userInfoFilter(val, 'realname');
          } else if (selectType == 'position') {
            return positionInfoFilter(val, 'posname');
          } else if (selectType == 'department') {
            return departmentInfoFilter(val, 'deptname');
          }
        };

        $scope.remove = function(val) {
          console.log('disabledRemove');
          // 如果初始已经选择了人员,不可删除 传值之间类型转换为
          if (attrs.disabledRemove === 'true') return
          var index = $scope.collection.indexOf(val);

          if (index !== -1) {
            $scope.collection.splice(index, 1);
            $ngModelCtrl.$setViewValue($scope.collection.join(','));
          }
        };

        $ngModelCtrl.$render = function() {
          if (angular.isString($ngModelCtrl.$viewValue)) {
            // 过滤空值
            $scope.collection = removeFilter($ngModelCtrl.$viewValue ? $ngModelCtrl.$viewValue.split(',') : [], '');
          } else {
            $scope.collection = [];
          }
        };
      }
    }
  };
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:userSelectorTrigger
 * @element   INPUT
 * @restrict  E
 * @requires  ngModel
 * @description
 *   人员选择控件触发器
 * @param     {string|number}  maxSelection    最大可选数
 * @param     {expression}     enabledUsers     可选 uid 数组或以 ',' 分隔的字符串
 */
.directive('userSelectorTrigger', function(UserSelector) {

  return {
    restrict: 'A',
    require: 'ngModel',
    link: function($scope, $element, $attr, $ngModelCtrl) {
      var userSelector;

      function osSelect(selected) {
        $ngModelCtrl.$setViewValue(selected.join(','));
        userSelector.hide();
      }

      function showUserSelector() {
        var enabled;
        var options;

        if ($attr.enabledUsers) {
          enabled = $scope.$eval($attr.enabledUsers);
          if (angular.isString(enabled)) {
            enabled = enabled.split(',');
          }
        }

        options = {
          values: $ngModelCtrl.$viewValue ? $ngModelCtrl.$viewValue.split(',') : [],
        };

        if (userSelector) {
          userSelector.show(options, osSelect);
        } else {
          var createOptions = {};
          if ($attr.maxSelection) {
            createOptions.maxSelection = $attr.maxSelection;
          }
          if (enabled) {
            createOptions.enabled = enabled;
          }

          UserSelector.create(createOptions).then(function(modal) {
            userSelector = modal;
            userSelector.show(options, osSelect);
          });
        }
      }

      $element.on('click', showUserSelector);

      $scope.$on('$destroy', function() {
        userSelector && userSelector.remove();
      });
    }
  };
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:userAvatarSelector
 * @element   INPUT
 * @restrict  E
 * @requires  ngModel
 *
 * @description
 *   选择用户，以头像的方式呈现；
 * @param     {String}  selectType             选择的数据类型
 * @param     {String}  userInfoFilter         数据过滤，过滤出要在列表项显示的数据
 */
.directive('userAvatarSelector', function($filter, removeFilter, userInfoFilter, departmentInfoFilter, positionInfoFilter) {
  return {
    restrict: 'E',
    //replace: true,
    require: 'ngModel',
    scope: {
      ngModel: '='
    },
    // transclude: true,
    template: '<div class="cslist" ng-click="outRemoveMode()">' +
      '<li ng-repeat="val in collection track by $index">' +
      '<i class="o40-minus-assertive-so" ng-show="removeMode" ng-click="remove($event, val)"></i>' +
      '<img avatar="{{ val | userInfo:\'avatar_middle\' | fullUrl }}" alt="{{ uid | userInfo:\'realname\' }}"/>' +
      '<div class="cslist-desc ellipsis">{{ val | userInfo: \'realname\'}} </div>' +
      '</li>' +
      '<li class="cslist-plus">' +
      '<div class="cslist-plus-icon">' +
      '<i class="ion-ios-plus-empty" user-selector-trigger ng-model="ngModel"></i>' +
      '</div>' +
      '</li>' +
      '<li class="cslist-plus" ng-show="!removeMode && collection.length" ng-click="toRemoveMode($event)">' +
      '<div class="cslist-plus-icon">' +
      '<i class="ion-ios-minus-empty"></i>' +
      '</div>' +
      '</li>' +
      '</div>',

    compile: function(element, attr) {
      // 过继各种有效属性到内部的 input
      var input = element.find('input');

      angular.forEach({
        'name': attr.name,
        'ng-value': attr.ngValue,
        'ng-model': attr.ngModel,
        'ng-disabled': attr.ngDisabled,
        'ng-change': attr.ngChange,
        'required': attr.required
      }, function(value, name) {
        if (angular.isDefined(value)) {
          input.attr(name, value);
        }
      });

      // Link
      return function($scope, $elem, attrs, $ngModelCtrl) {
        var selectType = attrs.selectType || 'user';

        $scope.getName = function(val) {
          if (selectType == 'user') {
            return userInfoFilter(val, 'realname');
          } else if (selectType == 'position') {
            return positionInfoFilter(val, 'posname');
          } else if (selectType == 'department') {
            return departmentInfoFilter(val, 'deptname');
          }
        };

        $scope.outRemoveMode = function(evt) {
          $scope.removeMode = false
        }

        $scope.removeMode = false;
        $scope.toRemoveMode = function($event) {
          $scope.removeMode = true;
          $event.stopPropagation();
        };

        $scope.remove = function($event, val) {
          var index = $scope.collection.indexOf(val);
          if (index !== -1) {
            $scope.collection.splice(index, 1);
            $ngModelCtrl.$setViewValue($scope.collection.join(','));
          }
          if (!$scope.collection.length) $scope.removeMode = false;
          $event.stopPropagation();
        };

        $ngModelCtrl.$render = function() {
          if (angular.isString($ngModelCtrl.$viewValue)) {
            // 过滤空值
            $scope.collection = removeFilter($ngModelCtrl.$viewValue ? $ngModelCtrl.$viewValue.split(',') : [], '');
          } else {
            $scope.collection = [];
          }
        };
      }
    }
  };
})


/**
 * @ngdoc    directive
 * @name     ibosApp.directives.directive:charcount
 * @element  ANY
 * @restrict E
 *
 * @description
 *   输入字数统计
 *
 * @param     {Number}   max           字数允许的最大值
 * @param     {Boolean}  countdown     是否倒数
 */
.directive('charcount', function(Utils) {
  return {
    restrict: 'E',
    require: 'ngModel',
    replace: true,
    scope: true,
    transclude: true, // 允许内嵌内容
    template: '<span class="charcount"> <em>{{count}}</em> <span ng-transclude></span> </span>',
    link: function($scope, elem, attrs, $ngModelCtrl) {
      // 字数允许的最大值
      var max = isNaN(+attrs.max) ? 0 : +attrs.max;
      // 是否倒数
      var countdown = attrs.countdown == 'true';
      $scope.count = countdown ? max : 0;

      $ngModelCtrl.$render = function() {
        var valCount = Utils.getCharLength($ngModelCtrl.$viewValue || '');
        $scope.count = countdown ?
          max - valCount :
          valCount;
        $ngModelCtrl.$setValidity('charcount', valCount <= max);
      };
    }
  }
})

.directive('ibDownload', function(Downloader) {

  return {
    restrict: 'A',
    compile: function($elem) {
      function prelink($scope, $element, $attrs) {
        $element.on('click', function() {
          Downloader.down($attrs.ibDownload);
        });
      }

      return {
        pre: prelink
      };
    }
  }
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:datetimeLocal
 * @element   ANY
 * @restrict  E
 * @requires  ngModel
 * @todo
 *   text 类型时，做格式验证，并同步到 ng-valid 状态
 * @description
 *   对于不支持 datetime-local 类型表单控件的浏览器，使用 text 代替
 */
.directive('datetimeLocal', function(Utils) {
  return {
    restrict: 'E',
    replace: true,
    require: 'ngModel',
    template: function() {
      return Utils.isDateTimeLocalSupport ? '<input type="datetime-local" />' : '<input type="text" />';
    },
    compile: function(element, attr) {
      angular.forEach({
        'name': attr.name,
        'ng-value': attr.ngValue,
        'ng-model': attr.ngModel,
        'ng-disabled': attr.ngDisabled,
        'ng-change': attr.ngChange,
        'required': attr.required,
        'placeholder': attr.placeholder
      }, function(value, name) {
        if (angular.isDefined(value)) {
          element.attr(name, value);
        }
      });
    }
  };
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:fileOnChange
 * @element   INPUT
 * @restrict  A
 * @description
 *   input[type="file"] 发生变化时触发相应回调
 *
 * @param     {expression}  fileOnChange  回调函数
 */
.directive('fileOnChange', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind('change', function() {
        var onChangeFunc = scope.$eval(attrs.fileOnChange);
        onChangeFunc.apply(element, arguments);
      });
    }
  };
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:fileImageData
 * @element   INPUT
 * @restrict  A
 * @todo      迁移至 file-util 模块
 * @description
 *   input[type="file"] 发生变化时，将选择的文件以 dataURL 的形式赋值到指定变量
 *
 * @param     {String}   fileImageData         用于保存 dataURL 的属性
 * @param     {Number}   fileImageMaxlength    最多允许选择的文件数
 */
.directive('fileImageData', function($parse, Utils) {
  var reg = /image\/(jpg|jpeg|png|bmp|gif)/;

  function testFileType(file) {
    if (!reg.test(file.type)) {
      alert('“' + file.name + '” 为不支持的文件格式 ' + file.type);
      return false;
    }
    return true;
  }

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var key = attrs.fileImageData;
      var maxLength = attrs.fileImageMaxlength;

      element.on('change', function($event) {
        var files = $event.target.files;
        var file;
        if (!files.length) {
          return;
        }

        // 单选时
        if (!maxLength) {
          file = files[0];

          if (!testFileType(file)) return;

          Utils.getImagePreviewUrl(file, function(url) {
              scope.$eval(key + ' = "' + url + '"');
              scope.$apply(key);
            })
            // 多选时，绑定模型必须为数组，否则报错
        } else {
          var images = $parse(key)(scope);

          if (!angular.isArray(images)) {
            console.error(key + ' 不是一个数组！');
            return;
          }

          var currentLength = images.length;

          for (var i = 0; i < files.length; i++) {
            file = files[i];
            if (!testFileType(file)) continue;
            // 判断最大选择数
            if (maxLength && currentLength >= maxLength) {
              alert('已达到最大选择数 ' + maxLength);
              break;
            }
            currentLength++;
            Utils.getImagePreviewUrl(file, function(url) {
              images.push(url);
              scope.$apply(key);
            });
          }
        }
      });
    }
  }
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:stopPropagation
 * @element   ANY
 * @restrict  A
 *
 * @description
 *  阻止指定节点上的指定事件冒泡
 *  stop-propagation="click,touch,tap"
 * @param    {String}   stopPropagation   以事件名组成的字符串，用“,”分隔
 */
.directive('stopPropagation', function() {
  function _stopPropagation(evt) {
    evt.stopPropagation();
  }

  return {
    restrict: 'A',
    link: function($scope, $element, $attrs) {
      var eventList = $attrs.stopPropagation;
      angular.forEach(eventList.split(','), function(eventName) {
        $element.on(eventName, _stopPropagation);
      });
    }
  };
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:focusOn
 * @element   input|textarea|select|element[contenteditable]
 * @restrict  A
 *
 * @description
 *  自动焦点到表单控件上
 *
 * @param     {expression}   focusOn    当此值为 true 时，表单控件将获得焦点
 */
.directive('focusOn', function($timeout) {
  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {
      $scope.$watch($attr.focusOn, function(shouldFocus) {
        $timeout(function() {
          if(shouldFocus) {
            $element[0].focus()
          }
        }, $scope.$eval($attr.focusDelay) || 0);
      });
    }
  };
})


/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:avatar
 * @element   ANY
 * @restrict  A
 *
 * @description
 *  头像，一开始设置为默认头像，读取真实头像成功后再替换
 *
 * @param    {string}   avatar                真实头像地址
 * @param    {string}   avatar-default-size   默认头像尺寸
 * @param    {string}   avatar-default-url    默认头像地址
 */
.directive('avatar', function() {
  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {
      // 默认头像尺寸
      var size = ['big', 'middle', 'small'].indexOf($attr.avatarDefaultSize) !== -1 ? $attr.avatarDefaultSize : 'middle';
      // 默认图像地址，有设置此值时，不使用默认头像
      var defaultUrl = $attr.avatarDefaultUrl || '/img/noavatar_' + size + '.png';

      if ($element[0].tagName.toLowerCase() === 'img') {
        $scope.$watch(function() {
          return $attr.avatar;
        }, function(url) {
          $element.attr('src', defaultUrl);
          if (url) {
            angular.element('<img />').on('load', function() {
                $element.attr('src', url);
              })
              .attr('src', url);
          }
        });
      }
    }
  };
})

.directive('attachList', function(fullUrlFilter, Downloader) {
  return {
    retrict: 'E',
    templateUrl: 'templates/attach.html',
    replace: true,
    scope: {
      'attach': '='
    },
    link: function($scope, $element, $attr) {
      $scope.download = function(att) {
        Downloader.down(fullUrlFilter(att.entireattachurl || att.officereadurl));
      };
    }
  }
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:transheader
 * @element   ANY
 * @restrict  A
 *
 * @description
 *  设置头部条为透明，并随着滚动位置变换颜色
 *
 */
.directive('transheader', function($ionicScrollDelegate) {

  return {
    restrict: 'A',
    link: function($scope, $element, $attrs) {
      var $ionContent = $element.find('ion-content');

      function toggleHeaderClass(status) {
        angular.forEach(document.querySelectorAll('ion-header-bar'), function(elem) {
          angular.element(elem).toggleClass('trans-header', status);
        });
      }

      function onScroll(evt) {
        toggleHeaderClass(evt.detail.scrollTop <= 0);
      }

      $scope.$on('$ionicView.enter', function() {
        toggleHeaderClass($ionicScrollDelegate.getScrollPosition().top <= 0);
        $ionContent.on('scroll', onScroll);
      });

      $scope.$on('$ionicView.leave', function() {
        toggleHeaderClass(false);
        $ionContent.off('scroll', onScroll);
      });
    }
  };
})

/**
 * @ngdoc     directive
 * @name      ibosApp.directives.directive:backState
 * @element   ANY
 * @restrict  A
 *
 * @description
 *  当没有历史时设置默认返回按钮
 *  `method:goBack()`    触发返回
 *  `method:getTitle()`  获取默认文本
 *
 */
.directive('defaultNavBackButton', function($ionicHistory, $ionicConfig, $ionicPlatform, DefaultBack) {

  return {
    link: link,
    restrict: 'EA'
  };

  function link(scope, element, attrs) {

    scope.backTitle = function() {
      var defaultBack = DefaultBack.get();
      if ($ionicConfig.backButton.previousTitleText() && defaultBack) {
        return $ionicHistory.backTitle() || defaultBack.title;
      }
    };

    scope.goBack = DefaultBack.goBack;

    scope.$on('$stateChangeSuccess', function() {
      element.toggleClass('hide', !DefaultBack.get());
    });

    // $ionicPlatform.registerBackButtonAction(function() {
    //   if ($ionicHistory.backView()) {
    //     $ionicHistory.goBack();
    //   } else if (DefaultBack.get()) {
    //     goDefaultBack();
    //   } else {
    //     navigator.app.exitApp();
    //   }
    // }, 100);

  }
})

/**
 * @ngdoc    directive
 * @name     ibosApp.directives.directive:clickInput
 * @element  DIV
 * @function
 * @restrict A
 * @description
 * 给input节点添加点击事件
 */
.directive('clickInput', function() {
  return {
    restrict: 'A',
    link: function($scope, $element, $arr) {
      $element.on('click', function(e) {
        var nodeName = e.target.nodeName.toLocaleLowerCase();
        // if (nodeName !== 'div' && nodeName === 'i') {
        //   e.target.parentNode.querySelector('input').click();
        // } else {
        //   e.target.parentNode.querySelector('input').click();
        // }
        e.target.parentNode.querySelector('input').click();
      });
    }
  };
})
