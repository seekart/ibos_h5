/**
 * @ngdoc module
 * @name docs.controllers
 * @description
 *   公文模块控制器
 */
angular.module('docs.controllers', [])

/**
 * @ngdoc controller
 * @name DocsPublishedCtrl
 * @module docs.controllers
 * @description
 *   已发布列表页控制器
 */
.controller('DocsPublishedCtrl', function($scope, $state, Settings, DocsPublished){
  $scope.list = DocsPublished;
  $scope.title = '已发布';
  $scope.$root.hideTabs = false;

  $scope.checkNoData = function() {
    return !$scope.list.docs.length && !$scope.list.hasMore;
  }

  $scope.open = function(n) {
    $state.go('docs.detail', { id: n.docid });
  }

  // 若没有页面缓存，重置查询条件，直接加载第一页的数据
  // 如果上一次的页面缓存为搜索结果，同样重置查询条件
  if(!$scope.list.hasDocs() || $scope.list.params.search) {
    $scope.list.fetch({
      catid: 0,
      page: 1,
      search: ''
    }, true)
  }

})

/**
 * @ngdoc controller
 * @name DocsUnsignedCtrl
 * @module docs.controllers
 * @description
 *   未签收列表页控制器
 */
.controller('DocsUnsignedCtrl', function($scope, $state, Settings, DocsUnsigned){
  $scope.list = DocsUnsigned;
  $scope.title = '未签收';
  $scope.$root.hideTabs = false;

  $scope.checkNoData = function() {
    return !$scope.list.docs.length && !$scope.list.hasMore;
  }

  $scope.open = function(n) {
    $state.go('docs.unsignedDetail', { id: n.docid });
  }

  // 若没有页面缓存，重置查询条件，直接加载第一页的数据
  // 如果上一次的页面缓存为搜索结果，同样重置查询条件
  if(!$scope.list.hasDocs() || $scope.list.params.search) {
    $scope.list.fetch({
      catid: 0,
      page: 1,
      search: ''
    }, true)
  }

})


/**
 * @ngdoc controller
 * @name DocsSignedCtrl
 * @module docs.controllers
 * @description
 *   已签收列表页控制器
 */
.controller('DocsSignedCtrl', function($scope, $state, Settings, DocsSigned){
  $scope.list = DocsSigned;
  $scope.title = '已签收';
  $scope.$root.hideTabs = false;

  $scope.checkNoData = function() {
    return !$scope.list.docs.length && !$scope.list.hasMore;
  }

  $scope.open = function(n) {
    $state.go('docs.signedDetail', { id: n.docid });
  }

  // 若没有页面缓存，重置查询条件，直接加载第一页的数据
  // 如果上一次的页面缓存为搜索结果，同样重置查询条件
  if(!$scope.list.hasDocs() || $scope.list.params.search) {
    $scope.list.fetch({
      catid: 0,
      page: 1,
      search: ''
    }, true)
  }

})

/**
 * @ngdoc controller
 * @name DocsCategoryCtrl
 * @module docs.controllers
 * @description
 *   分类控制器
 */
.controller('DocsCategoryCtrl', function($scope, $state, $stateParams, DocsPublished){
  $scope.list = DocsPublished;
  $scope.categorys = [];
  $scope.title = '按分类';
  $scope.rootDir = !($stateParams.cid && $stateParams.cid != 0);

  $scope.checkNoData = function() {
    return !$scope.list.news.length && !$scope.list.hasMore;
  };

  // 若没有页面缓存，重置查询条件，直接加载第一页的数据
  // 如果上一次的页面缓存为搜索结果，同样重置查询条件
  $scope.list.fetch({
    catid: $stateParams.cid,
    page: 1
  }, true)
  .success(function(res) {
    $scope.categorys = res.category;
  });

})

/**
 * @ngdoc controller
 * @name DocsPublishedCtrl
 * @module docs.controllers
 * @description
 *   新闻详细页控制器
 */
.controller('DocsDetailCtrl', function($scope, $sce, $filter, $stateParams, Docs) {
  $scope.docs = {};
  $scope.signing = false;

  $scope.sign = function(docid) {
    $scope.signing = true;
    Docs.sign(docid)
    .success(function(){ 
      $scope.signing = false;
      $scope.docs.issign = '1';
    })
    .error(function(){ $scope.signing = false; })
  }

  Docs.get($stateParams.id)
  .success(function(res) {
    $scope.docs = res.data;
    $scope.attach = res.attach;
    if($scope.docs.content) {
      $scope.docs.content = $sce.trustAsHtml( $filter('editorContent')($scope.docs.content) );
    }
  }); 
})