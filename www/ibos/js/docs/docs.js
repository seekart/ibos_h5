/**
 * @ngdoc module
 * @name docs
 * @description
 *   公文模块入口
 */

angular.module('docs', ['ionic', 'docs.controllers', 'docs.services'])

.run(function(Apps) {
  Apps.install({ name: 'docs', title: '公文', route: '#/docs', state: 'docs.published', icon: 'img/modicon/docs.png' });
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/docs', '/docs/published');

  $stateProvider
  // setup an abstract state for the tabs directive
  .state('docs', {
    url: "/docs",
    abstract: true,
    templateUrl: "templates/docs/docs.html",
    resolve: UserProvider.resolve
  })

  .state('docs.published', {
    url: '/published',
    talkingDataLabel: '公文-已发布',
    views: {
      'docs-published': {
        templateUrl: 'templates/docs/docs-list.html',
        controller: 'DocsPublishedCtrl'
      }
    }
  })

  .state('docs.unsigned', {
    url: '/unsigned',
    talkingDataLabel: '公文-未签收',
    views: {
      'docs-unsigned': {
        templateUrl: 'templates/docs/docs-list.html',
        controller: 'DocsUnsignedCtrl'
      }
    }
  })

  .state('docs.signed', {
    url: '/signed',
    talkingDataLabel: '公文-已签收',
    views: {
      'docs-signed': {
        templateUrl: 'templates/docs/docs-list.html',
        controller: 'DocsSignedCtrl'
      }
    }
  })

  .state('docs.category', {
    url: '/category?cid',
    talkingDataLabel: '公文-按分类',
    views: {
      'docs-category': {
        templateUrl: 'templates/docs/docs-category.html',
        controller: 'DocsCategoryCtrl'
      }
    }
  })

  .state('docs.detail', {
    url: '/detail/:id',
    talkingDataLabel: '公文-查看公文',
    views: {
      'docs-published': {
        templateUrl: 'templates/docs/docs-detail.html',
        controller: 'DocsDetailCtrl'
      }
    }
  })

  .state('docs.unsignedDetail', {
    url: '/unsignedDetail/:id',
    talkingDataLabel: '公文-查看公文',
    views: {
      'docs-unsigned': {
        templateUrl: 'templates/docs/docs-detail.html',
        controller: 'DocsDetailCtrl'
      }
    }
  })

  .state('docs.signedDetail', {
    url: '/signedDetail/:id',
    talkingDataLabel: '公文-查看公文',
    views: {
      'docs-signed': {
        templateUrl: 'templates/docs/docs-detail.html',
        controller: 'DocsDetailCtrl'
      }
    }
  })

  .state('docs.categoryDetail', {
    url: '/categoryDetail/:id',
    talkingDataLabel: '公文-查看公文',
    views: {
      'docs-category': {
        templateUrl: 'templates/docs/docs-detail.html',
        controller: 'DocsDetailCtrl'
      }
    }
  })

});