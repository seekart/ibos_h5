/**
 * @ngdoc module
 * @name email
 * @description
 *   邮件中心模块
 */

angular.module('email', ['ionic', 'email.controllers', 'email.services', /*'email.filters'*/])

.run(function(Apps) {
  Apps.install({ name: 'email', title: '邮件中心', route: '#/email', state: 'email.index', icon: 'img/modicon/email.png' });
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/email', '/email/index');

  $stateProvider
  .state('email', {
    url: '/email',
    abstract: true,
    template: '<ion-nav-view name="email"></ion-nav-view>',
    resolve: UserProvider.resolve
  })

  $stateProvider
  // 邮件中心-首页
  .state('email.index', {
    url: '/index',
    talkingDataLabel: '邮件-首页',
    views: {
      'email': {
        templateUrl: 'templates/email/email-index.html',
        controller: 'EmailIndexCtrl'
      }
    }
  })

  // 邮件中心-收件箱
  .state('email.inbox', {
    url: '/inbox',
    talkingDataLabel: '邮件-收件箱',
    views: {
      'email': {
        templateUrl: 'templates/email/email-inbox.html',
        controller: 'EmailInboxCtrl'
      }
    }
  })

  // 邮件中心-待办邮件
  .state('email.todo', {
    url: '/todo',
    talkingDataLabel: '邮件-待办',
    views: {
      'email': {
        templateUrl: 'templates/email/email-inbox.html',
        controller: 'EmailTodoCtrl'
      }
    }
  })

  // 邮件中心-发件箱（已发送）
  .state('email.outbox', {
    url: '/outbox',
    talkingDataLabel: '邮件-已发送',
    views: {
      'email': {
        templateUrl: 'templates/email/email-outbox.html',
        controller: 'EmailOutboxCtrl'
      }
    }
  })

  // 邮件中心-回收站（已删除）
  .state('email.trash', {
    url: '/trash',
    talkingDataLabel: '邮件-已删除',
    views: {
      'email': {
        templateUrl: 'templates/email/email-inbox.html',
        controller: 'EmailTrashCtrl'
      }
    }
  })

  // 邮件中心-查看邮件
  .state('email.detail', {
    url: '/detail/:id',
    talkingDataLabel: '邮件-查看邮件',
    views: {
      'email': {
        templateUrl: 'templates/email/email-detail.html',
        controller: 'EmailDetailCtrl'
      }
    }
  })

  // 邮件中心-查看已发送邮件
  .state('email.outboxDetail', {
    url: '/outboxDetail/:id',
    talkingDataLabel: '邮件-查看邮件',
    views: {
      'email': {
        templateUrl: 'templates/email/email-detail.html',
        controller: 'EmailOutboxDetailCtrl'
      }
    }
  })

  // 邮件中心-查看回收站邮件
  .state('email.trashDetail', {
    url: '/trashDetail/:id',
    talkingDataLabel: '邮件-查看邮件',
    views: {
      'email': {
        templateUrl: 'templates/email/email-detail.html',
        controller: 'EmailTrashDetailCtrl'
      }
    }
  })

  // 邮件-写邮件
  .state('email.create', {
    url: '/create?id&type',
    talkingDataLabel: '邮件-写邮件',
    views: {
      'email': {
        templateUrl: 'templates/email/email-create.html',
        controller: 'EmailCreateCtrl'
      }
    }
  })
});