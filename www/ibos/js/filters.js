/**
 * @ngdoc overview
 * @name  ibosApp.filters
 * @description
 *   通用过滤器
 */
;(function() {

var appFilter = angular.module('ibosApp.filters', [])

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:htmlToPlaintext
 * @function
 * @description
 *  清理内容中包含的 html 标签
 *
 * @param   {string}  text 源文本内容
 * @returns {string}  清理后的内容
 */
.filter('htmlToPlaintext', function() {
  return function(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
  };
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:fileSize
 * @function
 * @description
 *  将文件大小转化成带单位的更易阅读的格式
 * @param   {number}  size 文件大小
 * @param   {number=} digit 保留的小数位数
 * @returns {string}  带单位的文件大小
 */
.filter('fileSize', function() {

  var unit = ['B', 'KB', 'MB', 'GB'];

  return function(size, digit){
    size = +size || 0;
    digit = digit == null ?  2 : digit;
    
    var pow = 1;

    while(size > 1024){
      size = size / 1024;
      pow++;
    }

    return digit ? 
      size.toFixed(digit) + unit[pow - 1] :
      size + unit[pow - 1]
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:filetypeIcon
 * @function
 * @description
 *  根据服务器端返回的文件类型图标路径解析出在 app 中的图标路径
 *
 * @param   {string}  source 服务器端文件类型图标
 * @returns {string}  文件类型图标路径
 */
.filter('filetypeIcon', function() {
  return function(source, size){

    if(!source) return '';

    var start = source.lastIndexOf('/');
    var end = source.lastIndexOf('.');

    if(start !== -1){
      source = source.substring(start + 1, end);
    }

    if(size) {
      source += '_' + size;
    }

    return 'img/filetype/' + source + '.png';
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:fileIconByExtname
 * @function
 * @description
 *   根据后缀名获取对应图标地址
 *
 * @param   {string}  extname  后缀名
 * @param   {string}  size     尺寸
 * @returns {string}  文件类型图标路径
 */
.filter('fileIconByExtname', function(FileUtils) {
  return FileUtils.getFileIconByExtname;
})

.filter('fileIconByFilename', function(fileIconByExtnameFilter) {
  return function(filename, size) {
    if(typeof filename === 'string') {
      return fileIconByExtnameFilter(filename.split('.').pop(), size)
    }
    return ''
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:fullUrl
 * @function
 * @description
 *   由于部分后端输出的文件路径是相对服务器的
 *   此 filter 用于将其拼接为完整路径
 *
 * @param   {string}  source 原文件路径
 * @returns {string}  完整路径
 */
.filter('fullUrl', function(Settings) {
  var httpReg = /^https?:\/\//;
  return function(source) {
    source = source || '';
    if(httpReg.test(source)) {
      return source
    }
    return Settings.hostUrl + '/' + source;
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:editorContent
 * @function
 * @description
 *   编辑器文本，可能包括图片等资源地址，此处需要对这些地址做处理以指向正确资源
 *
 * @returns {string}  编辑器文本
 */
.filter('editorContent', function($filter) {
  return function(source) {
    if(!source) return source;

    var reg = /src\s*=\s*['"]\/?((data|static).*?)['"]/g;
    return source.replace(reg, function(a, b) {
      return 'src="' + $filter('fullUrl')(b) + '"';
    });
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:avatar
 * @function
 * @description
 *   根据 uid 从 User[service] 里获取头像
 *
 * @param   {string} uid   用于标识用户的唯一id
 * @param   {string} size  头像的尺寸，有大中小三种尺寸
 * @returns {string} 头像地址
 */
.filter('avatar', function($filter, User) {
  return function(uid, size) {
    size = size || 'middle';

    return uid && User.users[uid] ?
      $filter('fullUrl')(User.users[uid]['avatar_' + size]) :
      'img/noavatar_' + size + '.png';
  }
})

/**
 * @ngdoc filter
 *  @name ibosApp.filters.filter:userInfo
 * @function
 * @description
 *   根据 uid 获取用户某项信息
 *
 * @param   {string} uid   用于标识用户的唯一id; 格式为 '1,2,3,4...'
 * @param   {string} prop  需要获取的属性
 * @returns {string} 属性值
 */
.filter('userInfo', function(User) {
  return createGetProp(User.users);
})

.filter('removeUserPrefix', function(User) {
  return function(uids) {
    return User.removePrefix(uids);
  };
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:positionInfo
 * @function
 * @description
 *   根据 pid 获取岗位某项信息
 *
 * @param   {string} pid   用于标识岗位的唯一id; 格式为 '1,2,3,4...'
 * @param   {string} prop  需要获取的属性
 * @returns {string} 属性值
 */
.filter('positionInfo', function(User) {
  return createGetProp(User.positionData.datas);
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:departmentInfo
 * @function
 * @description
 *   根据 did 获取部门某项信息
 *
 * @param   {string} did   用于标识部门的唯一id; 格式为 '1,2,3,4...'
 * @param   {string} prop  需要获取的属性
 * @returns {string} 属性值
 */
.filter('departmentInfo', function(User) {
  return createGetProp(User.departmentData.datas);
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:asHtml
 * @function
 * @description
 *   作为安全的html显示
 *   angular会自动将不安全的 html 过滤，此 filter 用于保留所有 html 内容
 *   
 * @param   {string}       html  源 html
 * @returns {object}       生成 sce 对象
 */
.filter('asHtml', function($sce) {
  return function(html){
    return $sce.trustAsHtml(html);
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:httpPrefix
 * @function
 * @description
 *   补全网址的 http:// 前缀
 *   
 * @param   {string}       str  网址
 * @returns {string}       补全后的网址
 */
.filter('httpPrefix', function() {
  return function(str) {
    if(!str) return '';
    if(/^https?:\/\//i.test(str)) {
      return str;
    } else {
      return 'http://' + str;
    }
  };
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:defaultAvatar
 * @function
 * @description
 *   没有头像时获取默认头像
 *     
 * @param   {string}       src    头像地址
 * @param   {string}       size   默认头像尺寸 small, middle, big
 * @returns {string}       最终应显示的头像
 */
.filter('defaultAvatar', function() {
  return function(src, size) {
    size = size || 'middle';
    return src ? src : 'img/noavatar_' + size + '.png';
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:attachUrl
 * @function
 * @description
 *   附件真实地址
 *     
 * @param   {string}       url    原始地址
 * @returns {string}       预览地址
 */
.filter('attachUrl', function(Settings) {
  return function(url) {
    // @Todo: 此处希望后端统一一下数据格式 
    return url.indexOf('data/attachment/') === 0 ? Settings.hostUrl + url : url;
  }
})

/**
 * @ngdoc filter
 * @name ibosApp.filters.filter:currencyToNumber
 * @function
 * @description
 *   将货币格式的字符串转为数字
 *   主要作用是去除千分号
 * @param   {string}       currency    货币
 * @returns {number}       数字
 */
.filter('currencyToNumber', function() {
  function currencyToNumber(currency) {
    var ret = Number(currency)

    if(isNaN(ret)) {
      if(typeof currency === 'string') {
        return currencyToNumber(currency.replace(/,/g, ''))
      }
      return 0
    }

    return ret
  }

  return currencyToNumber  
})

function createGetProp(data) {
  return function(id, prop) {
    if(id == null) {
      return '';
    }
    // 转为字符串
    id = id + '';

    var idArr = id.split(',');

    if(idArr.length > 1) {
      
      return idArr.map(function(i) {
        return data[i] ? (prop ? data[i][prop] : data[i]) : '';
      }).join(',');

    } else {
      return data[id] ? (prop ? data[id][prop] : data[id]) : '';
    }
  }
}

})();
