/**
 * @ngdoc module
 * @name news
 * @description
 *   信息中心模块入口
 */

angular.module('news', ['ionic', 'news.controllers', 'news.services'/*, 'news.filters'*/])

.run(function(Apps) {
  Apps.install({ name: 'news', title: '新闻', route: '#/news', state: 'news.published', icon: 'img/modicon/news.png' });
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/news', '/news/published');

  $stateProvider
  // setup an abstract state for the tabs directive
  .state('news', {
    url: "/news",
    abstract: true,
    template:'<ion-nav-view name="news"></ion-nav-view>',
    resolve: UserProvider.resolve
  })

  .state('news.published', {
    url: '/published',
    talkingDataLabel: '新闻-已发布',
    cache: false,
    views: {
      'news': {
        templateUrl: 'templates/news/news-index.html',
        controller: 'NewsIndexCtrl'
      }
    }
  })

  .state('news.unread', {
    url: '/unread',
    talkingDataLabel: '新闻-未读',
    cache: false,
    views: {
      'news': {
        templateUrl: 'templates/news/news-unread.html',
        controller: 'NewsUnreadCtrl'
      }
    }
  })

    .state('news.unapproval', {
      url: '/unapproval',
      talkingDataLabel: '新闻-未审核',
      cache: false,
      views: {
        'news': {
          templateUrl: 'templates/news/news-unapproval.html',
          controller: 'NewsUnapprovalCtrl'
        }
      }
    })


  .state('news.category', {
    url: '/category?cid',
    talkingDataLabel: '新闻-按分类',
    cache: false,
    views: {
      'news': {
        templateUrl: 'templates/news/news-category.html',
        controller: 'NewsCategoryCtrl'
      }
    }
  })

  .state('news.detail', {
    url: '/detail/:id',
    talkingDataLabel: '新闻-查看新闻',
    views: {
      'news': {
        templateUrl: 'templates/news/news-detail.html',
        controller: 'NewsDetailCtrl'
      }
    }
  })

  .state('news.unreadDetail', {
    url: '/unreadDetail/:id',
    talkingDataLabel: '新闻-查看新闻',
    views: {
      'news': {
        templateUrl: 'templates/news/news-detail.html',
        controller: 'NewsDetailCtrl'
      }
    }
  })

  .state('news.readDetail', {
    url: '/readDetail/:id',
    talkingDataLabel: '新闻-查看新闻',
    views: {
      'news': {
        templateUrl: 'templates/news/news-detail.html',
        controller: 'NewsDetailCtrl'
      }
    }
  })

  .state('news.categoryDetail', {
    url: '/categoryDetail/:id',
    talkingDataLabel: '新闻-查看新闻',
    views: {
      'news': {
        templateUrl: 'templates/news/news-detail.html',
        controller: 'NewsDetailCtrl'
      }
    }
  })

    .state('news.approval', {
      url: '/approval/:id',
      talkingDataLabel: '新闻-审核新闻',
      views: {
        'news': {
          templateUrl: 'templates/news/news-approval.html',
          controller: 'NewsApprovalCtrl'
        }
      }
    })


});