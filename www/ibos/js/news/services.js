/**
 * @ngdoc overview
 * @name  news.services
 * @description
 *   新闻模块相关服务
 */
angular.module('news.services', [])

/**
 * @ngdoc service
 * @name  news.services.News
 * @description
 *   新闻模块数据交互及处理
 */
.factory('News', function($http, $state, $ionicLoading, Settings){

  var getModuleUrl = function () {
    return Settings.rootUrl + '/news';
  }

  return {
    /**
     * @ngdoc     method
     * @name      news.services.News#get
     * @methodOf  news.services.News
     * @description 
     *   获取单条新闻的数据
     * @param   {String}       id       新闻 id
     * @returns {HttpPromise}  HttpPromise
     */
    get: function(id) {
      $ionicLoading.show();

      return $http.get(getModuleUrl() + '/show', { params: { articleid: id } })
      .success($ionicLoading.hide)
      .error($ionicLoading.hide)
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#list
     * @methodOf  news.services.News
     * @description 
     *   获取新闻列表
     *   - `catid`：（可选）新闻分类
     *   - `search`：（可选）搜索
     *   - `type`：（可选）内容类型。new：未读、old：已读、待审核：notallow、草稿：draft
     *   - `page`：（可选）第几页，从0开始计算。默认值为0
     *   - `pagesize`：（可选）每页文章个数，默认值为10
     * @param   {Object}       params       请求参数
     * @returns {HttpPromise}  HttpPromise
     */
    list: function(params) {
      return $http.get(getModuleUrl() + '/index', { params: params })
    },

    /**
     * @ngdoc     method
     * @name      news.services.News##read
     * @methodOf  news.services.News
     * @description 
     *   改变新闻的状态为已读
     *   
     * @param   {String}       id     新闻 id
     * @returns {HttpPromise}  HttpPromise
     */
    read: function(id) {
      return $http.get(getModuleUrl() + '/read', { params: { articleid: id } })
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#open
     * @methodOf  news.services.News
     * @description 
     *   查看新闻详细
     * @param   {Object}       news         新闻数据
     * @param   {String}       detailState  详细页对应的 state
     * @returns {HttpPromise}  HttpPromise
     */
    open: function(news, detailState) {
      // 超链接类型
      if(news.type == '2') {
        window.open(news.url, '_blank', 'location=yes');
      // 文章、图片类型
      } else {
        $state.go(detailState, { id: news.articleid });
      }

      if(!news.readstatus) {
        this.read(news.articleid)
        .success(function(res) {
          if(res.isSuccess) news.readstatus = true;
        });
      }
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#category
     * @methodOf  news.services.News
     * @description
     *   获取新闻分类
     *   
     * @returns {HttpPromise}  HttpPromise
     */
    category: function(){
       return $http.get(getModuleUrl()+ '/category')
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#readall
     * @methodOf  news.services.News
     * @description
     *    标识所有未读新闻为已读
     *    
     * @returns {HttpPromise}  HttpPromise
     */
    readall: function(){
      return $http.post(getModuleUrl()+'/readall')
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#top
     * @methodOf  news.services.News
     * @description
     *    置顶新闻/取消置顶新闻
     *    请求参数说明：
     *    - `articleids`：新闻id。如果有多个，使用逗号分开。如：1,2,3
     *    - `topEndTime`：（可选）置顶过期时间。如果该值为空，则会取消置顶。
     *    
     * @param   {Object}       params       请求参数
     * @returns {HttpPromise}  HttpPromise
     */
    top: function(params){
      return $http.post(getModuleUrl()+'/top', params)
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#del
     * @methodOf  news.services.News
     * @description
     *    删除新闻
     *    请求参数说明：
     *    - `articleids`：新闻id。如果有多个，使用逗号分开。如：1,2,3
     * @param   {Object}       params       请求参数
     * @returns {HttpPromise}  HttpPromise
     */
    delete: function(params){
      return $http.post(getModuleUrl()+'/del', params);
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#getReaders
     * @methodOf  news.services.News
     * @description
     *    获取阅读人员列表
     *    请求参数说明：
     *    - `articleid`：新闻id
     *    
     * @param   {Object}       params       请求参数
     * @returns {HttpPromise}  HttpPromise
     */
    getReaders: function(params){
      return $http.post(getModuleUrl()+ '/getReaders', params);
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#verify
     * @methodOf  news.services.News
     * @description
     *    审批通过
     *    请求参数说明：
     *    - `articleids`：新闻id。如果有多个，使用逗号分开。如：1,2,3
     * @param   {Object}       params       请求参数
     * @returns {HttpPromise}  HttpPromise
     */
    verify: function(params){
      return $http.post(getModuleUrl()+ '/verify', params);
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#back
     * @methodOf  news.services.News
     * @description
     *    审批退回
     *    请求参数说明：
     *    - `articleids`：新闻id。如果有多个，使用逗号分开。如：1,2,3
     *    - `reason`：审批退回理由`
     * @param   {Object}       params       请求参数
     * @returns {HttpPromise}  HttpPromise
     */
    back: function(params){
      return $http.post(getModuleUrl()+ '/back',params)
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#addComment
     * @methodOf  news.services.News
     * @description
     *    添加评论
     *    请求参数说明：
     *    - `type`：评论类型。comment：评论、reply：回复。
     *    - `rowid`：如果是评论，则为新闻id；如果是回复评论，则为评论主体id。
     *    - `content`：评论内容
     *    - `tocid`：（可选）具体某条回复的id
     *    - `url`：（可选）链接地址
     *    - `detail`：（可选）详细来源信息描述
     * @param   {Object}       params       请求参数
     * @returns {HttpPromise}  HttpPromise
     */
    addComment: function(params){
      return $http.post(getModuleUrl()+ '/addComment', params)
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#getComments
     * @methodOf  news.services.News
     * @description
     *    获取评论列表
     *    
     * @param   {String}       articleid    新闻id
     * @returns {HttpPromise}  HttpPromise
     */
    getComments: function(params){
      return $http.get(getModuleUrl()+ '/getComments', { params : params } )
    },

    /**
     * @ngdoc     method
     * @name      news.services.News#move
     * @methodOf  news.services.News
     * @description
     *    移动新闻
     *    请求参数说明：
     *    - `articleids`：新闻id列表。如果有多个，使用逗号分开。如：1,2,3
     *    - `catid`：分类id
     * @param   {String}       articleid    新闻id
     * @returns {HttpPromise}  HttpPromise
     */
    move: function(params){
      return $http.post(getModuleUrl()+ '/move', params)
    }
  }
})

.factory('NewsList', function($http, $ionicLoading, Settings, Utils) {
  function NewsList(url, options){
    this.url = url;
    this.options = options;

    this.hasMore = true;
    this.news = [];

    this.params = {};
  }

  angular.extend(NewsList.prototype, {
    getUrl: function() {
      return Settings.rootUrl + this.url
    },
    /**
     * @ngdoc method
     * @name NewsList#fetch
     * @description
     *   获取文件列表数据
     *   返回数据格式为
     *   {
     *     datas: [
     *       { addtime, author, uid, subject, content, uptime, readstatus... }
     *       ...
     *     ],
     *     pages: { page, pageCount, pageSize }
     *   }
     *   addtime: "1409381646"
     *
     * @param {Obejct} param 查询条件
     *    catid      {String}         分类 id
     *    page       {Number}         页码
     *    keyword    {String}         搜索关键词
     * @returns {Object} promise
     */

    fetch: function(params, reset, extend) {
      var _this = this;
      if(reset) {
        this.news = [];
        this.hasMore = true;
      }

      if(extend === false) {
        this.params = params;
      } else {
        angular.extend(this.params, params);
      }

      return $http.get(this.getUrl(), {
        params: this.params
      })

      .success(function(res){
          if(res.isSuccess){
            if(res.pages.page < res.pages.pageCount) {
              _this.news = _this.news.concat(res.data);
              // _this.params.page += 1;
            }
            // 判断是否还能加载更多
            _this.hasMore = res.pages.page < res.pages.pageCount - 1
          } else {
            Utils.promptNoPermission();
          }
      })
    },

    search: function(keyword) {
      return this.fetch({
        page: 1,
        search: keyword
      }, true);
    },

    loadMore: function() {
      return this.fetch({
        page: this.params.page + 1
      });
    },

    hasNews: function() {
      return !!this.news.length;
    },

    reset: function() {
      this.news.length = 0;
      this.hasMore = true;
    }
  });

  return NewsList;
})

.factory('NewsIndex',function(NewsList,Settings){
  var list = new NewsList('/news/index');

  return list;
})

.factory('NewsUnread', function(NewsList, Settings) {
  var list = new NewsList('/news&type=new');

  return list;
})


