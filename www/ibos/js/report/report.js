/**
 * @ngdoc module
 * @name report
 * @description
 *   汇报模块入口
 */

angular.module('report', ['ionic', 'report.controllers', 'report.services'/*, 'report.filters'*/])

  .run(function(Apps) {
    Apps.install({ name: 'report', title: '汇报', route: '#/report', state: 'report.index', icon: 'img/modicon/report.png' });
  })

  .config(function($stateProvider, $urlRouterProvider, UserProvider, ResolversProvider) {

    $urlRouterProvider.when('/report', '/report/index');

    $stateProvider
      // setup an abstract state for the tabs directive
      .state('report', {
        url: "/report",
        abstract: true,
        template: '<ion-nav-view name="report"></ion-nav-view>',
        resolve: UserProvider.resolve
      })

      .state('report.index', {
        url: '/index',
        talkingDataLabel: '汇报-首页',
        views: {
          'report': {
            templateUrl: 'templates/report/report-index.html',
            controller: 'ReportIndexCtrl'
          }
        }
      })

      .state('report.form', {
        url: '/form/:repid?tid',
        talkingDataLabel: '汇报-表单（创建、编辑）',
        resolve: ResolversProvider.get(['qqmap']),
        views: {
          'report': {
            templateUrl: 'templates/report/report-form.html',
            controller: 'ReportFormCtrl'
          }
        }
      })

      .state('report.receive', {
        url: '/receive',
        cache: false,
        talkingDataLabel: '汇报列表-我收到的',
        views: {
          'report': {
            templateUrl: 'templates/report/report-receive.html',
            controller: 'ReportReceiveCtrl'
          }
        }
      })

      .state('report.send', {
        url: '/send',
        cache: false,
        talkingDataLabel: '汇报列表-我发出的',
        views: {
          'report': {
            templateUrl: 'templates/report/report-send.html',
            controller: 'ReportSendCtrl'
          }
        }
      })

      .state('report.view', {
        url: '/view/:repid',
        cache: false,
        talkingDataLabel: '汇报-详情',
        views: {
          'report': {
            templateUrl: 'templates/report/report-view.html',
            controller: 'ReportViewCtrl'
          }
        }
      })

  })