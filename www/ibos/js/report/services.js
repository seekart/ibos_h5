/**
 * @ngdoc overview
 * @name  report.services
 * @description
 *   汇报模块相关服务
 */
angular.module('report.services', [])

/**
 * @ngdoc service
 * @name report.services.Report
 * @description
 *   汇报通用服务
 */
  .factory('Report', function($http, Settings, Utils) {
    var getModuleUrl = function () {
      return Settings.hostUrl + '?r=report/api';
    }
    return {
      /**
       * @ngdoc     method
       * @name      report.services.report#usertemplate
       * @methodOf  report.services
       * @description
       *   获取用户可用模板
       * @returns {HttpPromise}  HttpPromise
       */
      getUserTemplate: function() {
        return $http.get(getModuleUrl() + '/usertemplate')
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#shoplist
       * @methodOf  report.services
       * @description
       *   获取商城列表
       * @returns {HttpPromise}  HttpPromise
       */
      getShopList: function() {
        return Utils.postJsonData(getModuleUrl() + '/shoplist')
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#sorttemplate
       * @methodOf  report.services
       * @description
       *   获取排序模板列表
       * @returns {HttpPromise}  HttpPromise
       */
      getSortTemplate: function() {
        return Utils.postJsonData(getModuleUrl() + '/sorttemplate')
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#addtemplate
       * @methodOf  report.services
       * @description
       *   添加系统模板
       * @returns {HttpPromise}  HttpPromise
       */
      addTemplate: function(tid){
        return Utils.postJsonData(getModuleUrl() + '/addTemplate',{ tid: tid })
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getManagerTemplate
       * @methodOf  report.services
       * @description
       *   获取管理模板列表
       * @returns {HttpPromise}  HttpPromise
       */
      getManagerTemplate: function(){
        return Utils.postJsonData(getModuleUrl() + '/managertemplate')
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getcharge
       * @methodOf  report.services
       * @description
       *   获取主管列表
       * @returns {HttpPromise}  HttpPromise
       */
      getcharge: function(){
        return Utils.postJsonData(getModuleUrl() + '/getcharge')
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getList
       * @methodOf  report.services
       * @description
       *   获取汇报列表
       * @returns {HttpPromise}  HttpPromise
       */
      getList: function(params){
        return Utils.postJsonData(getModuleUrl() + '/getlist',params)
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getTemplateData
       * @methodOf  report.services
       * @description
       *   设置/获取模板设置值
       * @returns {HttpPromise}  HttpPromise
       */
      settemplate: function(params){
        return Utils.postJsonData(getModuleUrl() + '/settemplate',params)
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getTemplateData
       * @methodOf  report.services
       * @description
       *   获取汇报模板信息（汇报添加页面信息）
       * @returns {HttpPromise}  HttpPromise
       */
      getTemplateData: function(data){
        return $http.get(getModuleUrl() + '/formreport',{ params: data })
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#savereport
       * @methodOf  report.services
       * @description
       *   保存汇报信息
       * @returns {HttpPromise}  HttpPromise
       */
      saveReport: function(params){
        return Utils.postJsonData(getModuleUrl() + '/savereport', params)
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#showReport
       * @methodOf  report.services
       * @description
       *   汇报详情
       * @returns {HttpPromise}  HttpPromise
       */
      showReport:function(repid){
        return $http.get(getModuleUrl() + '/showreport', { params:  { repid: repid } })
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#delReport
       * @methodOf  report.services
       * @description
       *   汇报详情
       * @returns {HttpPromise}  HttpPromise
       */
      delReport:function(repid){
        return Utils.postJsonData(getModuleUrl() + '/delreport', { repids: repid })
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#allRead
       * @methodOf  report.services
       * @description
       *   汇报详情
       * @returns {HttpPromise}  HttpPromise
       */
      allRead:function(repid){
        return Utils.postJsonData(getModuleUrl() + '/allread', { repids: 0 })
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#addComment
       * @methodOf  report.services
       * @description
       *   添加/回复评论
       * @returns {HttpPromise}  HttpPromise
       */
      addComment: function(params){
        return Utils.postJsonData(getModuleUrl() + '/addcomment', params )
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getCommentList
       * @methodOf  report.services
       * @description
       *   获取评论
       * @returns {HttpPromise}  HttpPromise
       */
      getComments: function(params){
        return Utils.postJsonData(getModuleUrl() + '/getcommentlist', params)
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getReaders
       * @methodOf  report.services
       * @description
       *   获取已读成员
       * @returns {HttpPromise}  HttpPromise
       */
      getReaders: function(repid){
        return Utils.postJsonData(getModuleUrl() + '/getreader', { repid : repid })
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getStamp
       * @methodOf  report.services
       * @description
       *   获取图章
       * @returns {HttpPromise}  HttpPromise
       */
      getStamp:function(repid){
        return $http.get(getModuleUrl() + '/getstamp')
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#setStamp
       * @methodOf  report.services
       * @description
       *   获取图章
       * @returns {HttpPromise}  HttpPromise
       */
      setStamp: function(params){
        return Utils.postJsonData(getModuleUrl() + '/setstamp', params)
      },

      /**
       * @ngdoc     method
       * @name      report.services.report#getUnreadCount
       * @methodOf  report.services
       * @description
       *   获取列表未读数
       * @returns {HttpPromise}  HttpPromise
       */
      getUnreadCount: function(){
        return $http.get(getModuleUrl() + '/getcount')
      }
    }
  })

/**
 * @ngdoc service
 * @name report.services.ReportUtils
 * @description
 *   汇报通用工具函数集
 */
  .factory('ReportUtils',function($state, External, IbPopup, dateFilter, CommonApi, Report){
    return{
      /**
       * @ngdoc     method
       * @name      report.services.getList#获取列表
       * @methodOf  report.services.ReportUtils
       * @param
       * @description
       * getList
       */
      getList: function(scope,params){
        Report.getList(params).success(function(res){
          if(res.isSuccess){
            scope.loading = false;
            scope.list = res.data.list;
            scope.list.hasMore= res.data.hasMore;
          }
        }).finally(function(){
          scope.$broadcast('scroll.refreshComplete');
        });
      }
    }
  })
