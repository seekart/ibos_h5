/**
 * @ngdoc overview
 * @name thread.controllers
 * @description
 *   主线模块控制器
 */
angular.module('thread.controllers', [])

.controller('ThreadIndexCtrl', function($scope, $ionicLoading, User, Thread, ThreadUtils, ThreadProgress, User, Utils) {
    $ionicLoading.show();
    User.getAuthority().success(function(res){
        if(res.isSuccess){
          if(res.data.thread){

            // 过滤出已关注的主线
            $scope.isAttention = function(th) {
              return th.isAttention;
            };

            // 获取剩余天数
            // 返回 0 时，表示未设置结束日期
            // 返回 -1 时，表示已过期
            $scope.getDayLeft = ThreadUtils.getDayLeft;

            // 判断负责人是否自己
            $scope.isInCharge = function(th) {
              return th.chargeuid == User.uid;
            };

            // 初始化列表，首次进来时自动加载数据
            $scope.init = function() {
              // 若没有页面缓存，重置查询条件，直接加载第一页的数据
              if(!$scope.list.hasItems()) {
                $scope.list.refresh();
              }
            };

            // 刷新列表
            $scope.refresh = function() {
              $scope.refreshing = true;
              $scope.list.refresh()
                .finally(function() {
                  $scope.$broadcast('scroll.refreshComplete');
                  $scope.refreshing = false;
                });
            };

            $scope.list = ThreadProgress;
            $scope.init();
          } else{
            Utils.promptNoPermission();
          }
        }
      }
    ).finally($ionicLoading.hide);
})

// 主线已结束列表
.controller('ThreadFinishedCtrl', function($scope, User, Thread, ThreadFinished) {
  // 初始化列表，首次进来时自动加载数据
  $scope.init = function() {
    // 若没有页面缓存，重置查询条件，直接加载第一页的数据
    if(!$scope.list.hasItems()) {
      $scope.list.refresh();
    }
  };

  // 刷新列表
  $scope.refresh = function() {
    $scope.refreshing = true;
    // $scope.prop.keyword = '';
    $scope.list.refresh()
    .finally(function() {
      $scope.$broadcast('scroll.refreshComplete');
      $scope.refreshing = false;
    });
  };

  $scope.list = ThreadFinished;
  $scope.init();

  // 判断负责人是否自己
  $scope.isInCharge = function(th) {
    return th.chargeuid == User.uid;
  };
})

// 主线详情页
.controller('ThreadViewCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, Thread, ThreadUtils) {
  $scope.thread = {
    starttime: 0,
    endtime: 0
  };

  // 获取主线本身数据
  Thread.view($stateParams)
  .success(function(res) {
    $scope.thread = res.datas.thread;
    $scope.dayLeft = ThreadUtils.getDayLeft($scope.thread.endtime);
  });

  // 幻灯切换
  $scope.slideChanged= function(index){
    $ionicSlideBoxDelegate.update();
  }
  $scope.$watch('thread.description', $scope.slideChanged.bind($scope));

  // 获取团队成员数据
  $scope.members = [];
  Thread.view({ id: $stateParams.id, tab: 'team' })
  .success(function(res) {
    $scope.members = setChargeFirstInMembers(res.datas.members);
  });

  // 找出负责人，并插入到数组首位
  function setChargeFirstInMembers(members) {
    for(var i = 0; i < members.length; i++) {
      if(members[i].isCharge) {
        members.unshift(members.splice(i, 1)[0]);
        break;
      }
    }
    return members;
  }

  $scope.tab = 'assignment';

  function ThreadRalatedList() {
    this.loaded = false;
    this.loading = false;
    this.more = true;
    this.list = [];

    this.load = function(params) {
      var that = this;
      if(this.loaded) return;

      this.loading = true;
      Thread.view(params)
      .success(function(res) {
        that.list = res.datas;
        that.loaded = true;
      })
      .finally(function() {
        that.loading = false;
      });
    }
  }

  // 任务列表
  $scope.assignment = new ThreadRalatedList();

  // 动态列表
  $scope.feed = new ThreadRalatedList();

  // 邮件列表
  $scope.email = new ThreadRalatedList();

  // 审批列表
  $scope.flow = new ThreadRalatedList();

  // 文件列表
  $scope.file = new ThreadRalatedList();

  $scope.$watch('tab', function(val) {
    $scope[val].load({ id: $stateParams.id, tab: val });
  })

  $scope.setTab = function(tab) {
    $scope.tab = tab;
  }

});