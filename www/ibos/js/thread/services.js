/**
 * @ngdoc module
 * @name thread.services
 * @description
 *   主线模块服务
 */

angular.module('thread.services', [])

.factory('ThreadUtils', function() {
  return {
    // P.s. 此处的 time 是以秒为单位的时间戳
    getDayLeft: function(time) { 
      time = +time;
      if(!time) return 0;
      return Math.ceil((time - Date.now() / 1000) / 86400);
    }
  };
})


/**
 * @ngdoc service
 * @name Thread
 * @module thread.services
 * @description
 *   主线数据服务
 */
.factory('Thread', function($http, Settings) {
  return {
    list: function(params) {
      return $http.get(Settings.rootUrl + '/thread/index', {
        params: params
      });
    },

    view: function(params) {
      return $http.get(Settings.rootUrl + '/threaddetail/detail', {
        params: params
      });
    }
  };
})

/**
 * @ngdoc service
 * @name AssignmentFinished
 * @module assignment.services
 * @description
 *   主线已完成列表数据
 */
.factory('ThreadProgress', function(Settings, IbList) {
  var list = new IbList('/thread/index&status=0', {
    idAttr: 'threadid'
  });

  list.showLoading = list.hideLoading = angular.noop;

  return list;
})


/**
 * @ngdoc service
 * @name AssignmentFinished
 * @module assignment.services
 * @description
 *   主线已完成列表数据
 */
.factory('ThreadFinished', function(Settings, IbList) {
  var list = new IbList('/thread/index&status=1', {
    idAttr: 'threadid'
  });
  list.showLoading = list.hideLoading = angular.noop;

  // assignmentFinished.search = function(keyword) {
  //   if(keyword.trim() === '') {
  //     return this.fetch({
  //       offset: 0,
  //     }, true);
  //   } else {
  //     return this.fetch({
  //       offset: 0,
  //       param: 'search',
  //       keyword: keyword
  //     }, true);
  //   }

  // };

  return list;
})
