/**
 * @ngdoc module
 * @name thread
 * @description
 *   主线模块
 */

angular.module('thread', ['ionic', 'thread.services', 'thread.controllers'])

.run(function(Apps) {
  Apps.install({ name: 'thread', title: '主线', route: '#/thread/index', state: 'thread.index', icon: 'img/modicon/thread.png' });
})

.config(function($stateProvider, $urlRouterProvider, UserProvider) {

  $urlRouterProvider.when('/thread', '/thread/index');

  $stateProvider
  .state('thread', {
    url: '/thread',
    abstract: true,
    template: '<ion-nav-view name="thread"></ion-nav-view>',
    resolve: UserProvider.resolve
  })

  $stateProvider
  // 主线首页
  .state('thread.index', {
    url: '/index',
    talkingDataLabel: '主线-首页',
    views: {
      'thread': {
        templateUrl: 'templates/thread/index.html',
        controller: 'ThreadIndexCtrl'
      }
    }
  })

  // 主线 -已结束的主线
  .state('thread.finished', {
    url: '/finished',
    talkingDataLabel: '主线-已结束',
    views: {
      'thread': {
        templateUrl: 'templates/thread/finished.html',
        controller: 'ThreadFinishedCtrl'
      }
    }
  })

  // 主线-详细页
  .state('thread.view', {
    url: '/view/:id',
    talkingDataLabel: '主线-查看主线',
    views: {
      'thread': {
        templateUrl: 'templates/thread/view.html',
        controller: 'ThreadViewCtrl'
      }
    }
  })
});