/**
 * @ngdoc module
 * @name vote.controllers
 * @description
 *   投票模块控制器
 */
angular.module('vote.controllers', [])

/**
 * @ngdoc controller
 * @name VoteIndexCtrl
 * @module vote.controllers
 * @description
 *   投票主页控制器
 */
.controller('VoteIndexCtrl', function($scope, $state, VoteUtils, Vote) {
  $scope.statusTab = 'underway';
  $scope.activeTab = 'nonparticipator';
  $scope.stateName = $state.current.name;
  $scope.loading = true;

  var params = {};

  $scope.setParticipationTab = function(type) {
    $scope.list = [];
    $scope.loading = true;
    $scope.activeTab = type || 'nonparticipator';
    params.type = $scope.activeTab === 'nonparticipator' ? 1 : 2;
    VoteUtils.voteList($scope, params)
  };

  $scope.setStatusTab = function(type) {
    $scope.statusTab = type || 'underway';
    params.type = $scope.statusTab !== 'underway' ? 5 : 4;
    VoteUtils.voteList($scope, params);
  };


  if($scope.stateName === 'vote.mine') {
    $scope.statusTab = 'underway';
    params.type = 4;
  } else {
    $scope.activeTab = 'nonparticipator';
    params.type = 1;
  }

  $scope.refresh = function() {
    if($scope.stateName === 'vote.index'){
     params.type = $scope.activeTab === 'nonparticipator' ? 1 : 2;
    } else {
     params.type = $scope.statusTab === 'underway' ? 4 : 5;
    }
    VoteUtils.voteList($scope, params);
  };

  VoteUtils.voteList($scope, params);
})

/**
 * @ngdoc controller
 * @name VoteViewCtrl
 * @module vote.controllers
 * @description
 *   投票详情页控制器
 */
.controller('VoteViewCtrl', function($scope, $timeout, $ionicModal, $ionicLoading, $stateParams, IbPopup, VoteUtils, Vote) {
  $scope.isOpenMore = false;
  $scope.openMore = function() {
    $scope.isOpenMore = true;
  };

  $scope.openCrewModal = function() {
    $ionicModal.fromTemplateUrl('templates/vote/vote-crew-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      })
      .then(function(modal) {
        $scope.crewModal = modal;
        $scope.crewModal.show();
        Vote.showvoteusers($stateParams.id).success(function(res) {
            $ionicLoading.show()
            if (res.isSuccess) {
              $scope.voteUsers = res.data.users.joined;
            }
          })
          .finally($ionicLoading.hide);
      })
  };

  VoteUtils.showVote($scope, $stateParams.id);

  var lastSelected = {};
  $scope.checkMaxSelected = function(topicid, max, currentItemid) {
    var topic = $scope.form.topics[topicid];
    var checkedNum = 0;

    // 获取当前选中的数目
    angular.forEach(topic, function(isChecked, itemid) {
      if (isChecked) checkedNum++;
    });

    if (checkedNum > max) {
      var lastItemid = lastSelected[topicid];
      topic[lastItemid] = false;
    }
    lastSelected[topicid] = currentItemid;
  };

  $scope.form = {
    voteid: ''
  };

  // 检查表单是否可以提交
  $scope.validForm = function() {
    var topics = VoteUtils.mainConvert($scope.form.topics);
    return topics.length === $scope.topics.length;
  };

  $scope.submit = function() {
    Vote.vote({
      vote: {
        voteid: $stateParams.id,
        topics: VoteUtils.arrayToObject(VoteUtils.mainConvert($scope.form.topics))
      }
    }).success(function(res) {
      if (res.isSuccess) {
        VoteUtils.showVote($scope, $stateParams.id);
      } else {
        IbPopup.alert({ title: res.msg });
      }
    })
  };

});