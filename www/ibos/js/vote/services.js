/**
 * @ngdoc overview
 * @name vote.services
 * @description
 *   投票模块服务
 */

angular.module('vote.services', [])


/**
 * @ngdoc service
 * @name vote.services.Diary
 * @description
 *   投票通用服务
 */

  .factory('Vote', function($http, Settings) {
    var getModuleUrl = function() {
      return Settings.hostUrl + '?r=vote';
    }

    return {
      /**
       * @ngdoc     method
       * @name      diary.services.Vote#VoteList
       * @methodOf  diary.services.Vote
       * @param     {Object}   params  type 1未参与，2已参与，3全部，4我发起的-进行中，5我发起的-已结束，6我发起的-全部。默认值3（全部）
       *
       * @description
       *   获取投票列表
       *
       * @returns   {HttpPromise}   HttpPromise
       */
      voteList: function (params) {
        return $http.get(getModuleUrl() + '/default/fetchindexlist', { params : params})
      },

      /**
       * @ngdoc     method
       * @name      diary.services.Vote#showVote
       * @methodOf  diary.services.Vote
       * @param     {Object}   params
       *             voteid  投票id
       *
       * @description
       *   查看投票
       * @returns   {HttpPromise}   HttpPromise
       */
      showVote: function (id) {
        return $http.get(getModuleUrl() + '/default/showvote', { params : { voteid : id }} )
      },

      /**
       * @ngdoc     method
       * @name      diary.services.Vote#vote
       * @methodOf  diary.services.Vote
       * @param     {Object}   params
       *             voteid  投票id
       *             topicid    投票话题id
       *             itemid    	投票选项id
       *
       * @description
       *   查看投票
       * @returns   {HttpPromise}   HttpPromise
       */
      vote: function (params) {
        return $http.post(getModuleUrl() + '/default/vote', params )
      },

      /**
       * @ngdoc     method
       * @name      diary.services.Vote#showvoteusers
       * @methodOf  diary.services.showvoteusers
       * @param     {Object}   params
       *             voteid  投票id
       *
       *
       * @description
       *   查看投票用户
       * @returns   {HttpPromise}   HttpPromise
       */
      showvoteusers: function (voteid) {
        return $http.get(getModuleUrl() + '/default/showvoteusers', { params: { voteid : voteid }} )
      },

    }
  })

  .factory('VoteUtils', function($ionicLoading, Utils, Vote) {

    return{
      voteList: function(scope, params) {
        $ionicLoading.show();
        Vote.voteList(params).success(function(res) {
          if(res.isSuccess){
            scope.loading = false;
            scope.list = res.data;
          } else {
            Utils.promptNoPermission();
          }
        }).finally(function(){
          $ionicLoading.hide();
          scope.$broadcast('scroll.refreshComplete');
        })
      },

     showVote: function(scope,voteid) {
       $ionicLoading.show();
       Vote.showVote(voteid).success(function(res) {
         if(res.isSuccess) {
           scope.vote = res.data.vote;
           scope.topics = res.data.topics;
           //scope.voteUsers = res.data.users.joined;
         }
       })
         .finally($ionicLoading.hide);
     },
      //子对象
      convertSubItem: function(value) { // { 22: true, 23: false, 24: true } => [22, 24]
        var result = [];
        for (var key in value){
          if(value[key]) {
            result.push(key);
          }
        }
        return result;
      },

      //转成数组
      mainConvert: function(topicsObj) {
        var result = [];
        var that = this;
        angular.forEach(topicsObj, function(value, topicid) {
          var itemIds = typeof value === 'number' ? [value] : typeof value === 'object' ? that.convertSubItem(value) : [];
          if(itemIds.length) {
            result.push({
              topicid: topicid,
              itemids: itemIds
            });
          }
        });
        return result;
      },

      arrayToObject: function(arr) {
        var ret = {};
        angular.forEach(arr, function(value, key) {
          ret[key] = value;
        });
        return ret;
      }
    }
  })
