
/**
 * @ngdoc module
 * @name vote
 * @description
 *   投票
 */

angular.module('vote', ['ionic', 'vote.controllers', 'vote.services'])

  .run(function(Apps) {
    Apps.install({ name: 'vote', title: '投票', route: '#/vote', state: 'vote.index', icon: 'img/modicon/vote.png' });
  })

  .config(function($stateProvider, $urlRouterProvider, UserProvider) {

    $urlRouterProvider.when('/vote', '/vote/index');


    $stateProvider
      .state('vote', {
        url: "/vote",
        abstract: true,
        template: '<ion-nav-view name="vote"></ion-nav-view>',
        resolve: UserProvider.resolve
      })

      .state('vote.index', {
        url: '/index',
        cache: false,
        talkingDataLabel: '投票-主页',
        views: {
          'vote': {
            templateUrl: 'templates/vote/vote-index.html',
            controller: 'VoteIndexCtrl'
          }
        }
      })

      .state('vote.mine', {
        url: '/mine',
        cache: false,
        talkingDataLabel: '投票-我的',
        views: {
          'vote': {
            templateUrl: 'templates/vote/vote-index.html',
            controller: 'VoteIndexCtrl'
          }
        }
      })

      //投票详情页
      .state('vote.view', {
        url: '/view/:id',
        talkingDataLabel: '投票-详情页',
        cache: false,
        views: {
          'vote': {
            templateUrl: 'templates/vote/vote-view.html',
            controller: 'VoteViewCtrl'
          }
        }
      })

  });