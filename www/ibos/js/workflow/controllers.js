/**
 * @ngdoc module
 * @name workflow.controllers
 * @description
 *   工作流模块控制器
 */
angular.module('workflow.controllers', [])

.controller('FlowIndexCtrl', function($scope,$ionicLoading, User, Utils) {
    $ionicLoading.show();
    User.getAuthority().success(function(res){
        if(res.isSuccess){
          if(!res.data.workflow) {
            Utils.promptNoPermission();
          }
        }
      }
    ).finally($ionicLoading.hide);
  })

.controller('FlowListCtrl', function($scope, $state, $ionicScrollDelegate, $ionicActionSheet) {
  $scope.prop = {};
  
  // 初始化列表，首次进来时自动加载数据
  $scope.init = function() {
    // 若没有页面缓存，重置查询条件，直接加载第一页的数据
    if(!$scope.list.hasItems()) {
      $scope.list.refresh();
    }
  }

  // 刷新列表
  $scope.refresh = function() {
    $scope.prop.keyword = '';
    $scope.list.refresh()
    .finally(function() {
      $scope.$broadcast('scroll.refreshComplete');
    })
  }

  $scope.sref = 'workflow.detail({ key: flow.key })';

  $scope.actionButtons = [];
  $scope.showMenu = function(flow) {
    var conf = {
      titleText: '工作流操作',
      buttons: $scope.actionButtons,
      buttonClicked: function(index, opts) {
        return opts.handler.call(null, flow);
      },
      cancelText: '取消'
    };

    if(angular.isFunction($scope.remove)) {
      conf.destructiveText = '删除';
      conf.destructiveButtonClicked = function() {
        return $scope.remove(flow);
      }
    }

    $scope.hideMenu = $ionicActionSheet.show(conf);
  }
})

/**
 * @ngdoc controller
 * @name FlowTodoCtrl
 * @module workflow.controllers
 * @description
 *   待办工作列表页控制器
 */
.controller('FlowTodoCtrl', function($scope, $controller, $state, $ionicActionSheet, IbPopup, Flow, FlowTodo) {

  $controller('FlowListCtrl', { $scope: $scope });
  $scope.list = FlowTodo;
  $scope.title = '待办工作';
  $scope.init();

  $scope.sref = 'workflow.handle({ key: flow.key })';

  var _showMenu = $scope.showMenu;

  $scope.showMenu = function(flow) {
    $scope.actionButtons = [];

    $scope.remove = flow.del ? function(flow) {
      Flow.confirmRemove(flow);
      return true;
    } : false;

    _showMenu(flow);
  }
})

/**
 * @ngdoc controller
 * @name FlowTransCtrl
 * @module workflow.controllers
 * @description
 *   已办结列表页控制器
 */
.controller('FlowTransCtrl', function($scope, $controller, Flow, FlowTrans) {

  $controller('FlowListCtrl', { $scope: $scope });
  $scope.list = FlowTrans;
  $scope.title = '已办结';
  $scope.init();

  $scope.takeback = function(flow) {
    Flow.confirmTakeback(flow);
    return true;
  };

  var _showMenu = $scope.showMenu;
  $scope.showMenu = function(flow) {
    $scope.actionButtons = [];

    // 若允许收回
    if(flow.rollback) {
      $scope.actionButtons.push({ 
        text: '收回',
        handler: function(flow) {
          return $scope.takeback(flow);
        } 
      })
    }
    
    _showMenu(flow);
  }
})

/**
 * @ngdoc controller
 * @name FlowDoneCtrl
 * @module workflow.controllers
 * @description
 *   已完成列表页控制器
 */
.controller('FlowDoneCtrl', function($scope, $controller, Flow, FlowDone) {

  $controller('FlowListCtrl', { $scope: $scope });
  $scope.list = FlowDone;
  $scope.title = '已完成';
  $scope.init();

  var _showMenu = $scope.showMenu;

  $scope.showMenu = function(flow) {
    $scope.actionButtons = [];

    $scope.remove = flow.del ? function(flow) {
      Flow.confirmRemove(flow);
      return true;
    } : false;

    _showMenu(flow);
  }
})

/**
 * @ngdoc controller
 * @name FlowFollowCtrl
 * @module workflow.controllers
 * @description
 *   我的关注列表页控制器
 */
.controller('FlowFollowCtrl', function($scope, $controller, FlowFollow) {

  $controller('FlowListCtrl', { $scope: $scope });
  $scope.list = FlowFollow;
  $scope.title = '我的关注';
  $scope.init();

  $scope.showMenu = angular.noop;
})

/**
 * @ngdoc controller
 * @name FlowCreateCtrl
 * @module workflow.controllers
 * @description
 *   发起工作页控制器
 */
.controller('FlowCreateCtrl', function($scope, $rootScope, $state, $ionicModal, $ionicLoading, $ionicHistory, IbPopup, Flow) {

  $scope.processes = [];
  $scope.categories = [];

  $scope.create = function(flow) {
    $ionicLoading.show();
    return Flow.create(flow.flowid)
    .success(function(res) {
      if(res.isSuccess) {
        $ionicHistory.clearCache(['workflow.todo']).then(function() {
          $state.go('workflow.handle', { key: res.key });
        });
      } else {
        IbPopup.alert({ title: res.msg });
      }
    })
    .finally($ionicLoading.hide);
  }

  // 查看分类下的流程
  var cateModal;
  // 为 Modal 新建一个 scope
  var cateModalScope = $rootScope.$new(true);
  cateModalScope.processes = [];
  cateModalScope.close = function() {
    cateModal && cateModal.hide();
  }
  cateModalScope.create = $scope.create;

  // 创建分类详细 Modal
  $ionicModal.fromTemplateUrl('./templates/workflow/workflow-cate-modal.html', {
    scope: cateModalScope,
    animation: 'slide-in-up',
  })
  .then(function(modal) {
    cateModal = modal;
  })

  // 查看分类下的流程
  $scope.openCate = function(cate) {
    cateModalScope.category = cate;
    cateModal.show();
  };

  $scope.$on('$ionicView.leave', function() {
    cateModal && cateModal.hide();
  });
  $scope.$on('$destroy', function() {
    cateModal && cateModal.remove();
  });

  // 获取可用的流程列表
  $scope.loading = true;
  Flow.getProcess()
  .success(function(res) {
    $scope.processes = res.common;
    $scope.categories = res.cate;
  })
  .finally(function() {
    $scope.loading = false;
  });
})

/**
 * @ngdoc controller
 * @name FlowHandleCtrl
 * @module workflow.controllers
 * @description
 *   办理工作页控制器
 */
.controller('FlowHandleCtrl', function($scope, $state, $stateParams, $timeout, $window, $ionicLoading, $ionicPopup, $ionicHistory, $ionicModal, $q, $filter, IbPopup, Settings, User, Flow, FlowUtils, icFactory, FlowUpload, FileUtils, IbPopup) {

  var flow = $scope.flow = {};
  var form = $scope.form = {};

  $scope.key = $stateParams.key;
  $scope.formhash = User.formhash;

  // 判断是否只读字段
  $scope.isReadonly = function(item) {
    if(flow.readonly) {
      return flow.readonly.split(',').indexOf(item['data-id']) !== -1;
    }
    return false;
  }

  // 查看原表
  $scope.viewSource = function() {
    $window.open(Settings.hostUrl + '?r=workflow/form/index&key=' + $scope.key, '_blank', 'location=yes');
  }

  function validateTitle(formData) {
    if (!formData.title.trim()) {
      IbPopup.alert({ title: '请输入标题' })
      return false
    }
    return true
  }

  function save() {
    form.attachmentid = $scope.imageAttach.concat($scope.fileAttach)
      .map(function(item) {
        return item.aid;
      })
      .join(',');
    var formData = icFactory.serialize(angular.extend({}, form), flow.enableArr);
    
    if (!validateTitle(formData)) return

    $ionicLoading.show();
    return Flow.saveFlow(formData)
    .success($ionicLoading.hide)
    .error($ionicLoading.hide);
  }

  // 保存表单数据
  $scope.save = function() {
    save()
    .success(function() {
      IbPopup.tip('保存成功');
    });
  };

  // 转交下一步
  $scope.handover = function() {
    save()
    .success(function() {
      // 自由流程的转交与固定流程转交不一样
      var state = $scope.flow.flow.type == '2' ? 'workflow.handoverFree' : 'workflow.handover';
      $state.go(state, { key: $scope.key, topflag: $scope.flow.rp.topflag });
    });
  };

  $scope.rollbackData = {};

  function saveRollback(params) {
    $ionicLoading.show();
    Flow.rollback($scope.key, params)
    .success(function(res) {
      if(res.isSuccess) {
        IbPopup.alert({ title: '成功退回' }).then(function(){
          $ionicHistory.clearCache(['workflow.todo', 'workflow.trans']).then(function() {
            $ionicHistory.goBack();
          });
        });
      } else {
        IbPopup.alert({ title: res.msg }).then(function() {
          $ionicHistory.goBack()
        });
      }
    })
    .finally($ionicLoading.hide);
  }

  function showRollBackModel(remind) {
    if($scope.flow.backlist && $scope.flow.backlist.length) {

      $scope.rollbackData.stepId = $scope.flow.backlist[0].id;

      if($scope.rollbackModal) {
        $scope.rollbackModal.show();
      } else {
        $ionicModal.fromTemplateUrl('templates/workflow/backstep-modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          modal.show();

          $scope.rollbackModal = modal;
          // 确定回退
          $scope.rollbackModal.save = function () {
            saveRollback({
              remind: remind,
              formhash: User.formhash,
              id: $scope.rollbackData.stepId
            });
            modal.hide();
          }
          
          $scope.$on('$destroy', function() {
            modal.remove();
            delete $scope.rollbackModal;
          });

          $scope.$on('$ionicView.leave', function() {
            modal.hide();
          });

        });
      }
    } else {
      // 没有可选回退步骤
    }
  }

  // 回退，按后端设置可分为直接回退到上一步，跟回退到可选步骤两种
  $scope.rollback = function() {
    
    $ionicPopup.show({
      template: '<input type="text" ng-model="rollbackData.remind">',
      title: '请输入回退原因',
      scope: $scope,
      buttons: [
        { text: '取消' },
        {
          text: '确定',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.rollbackData.remind) {
              e.preventDefault();
            } else {
              return $scope.rollbackData.remind;
            }
          }
        },
      ]
    })
    .then(function(res) {
      if(res) {
        // 回退到上一步
        if ($scope.flow.process.allowback == '1') {
          saveRollback({ remind: res, formhash: User.formhash });
        } else if ($scope.flow.process.allowback == '2') {
          // 回退到可选步骤
          showRollBackModel(res);
        }
      }
    });  
    
  }

  // 经办人会签（办理完毕）
  $scope.sign = function() {
    IbPopup.confirm({ title: '确定该工作已经办结吗？'})
    .then(function(res) {
      if(res) {
        $ionicLoading.show();
        Flow.signFlow($scope.key, {
            topflag: flow.rp.topflag,
            opflag: flow.rp.opflag,
            content: $scope.form.content
        })
        .success(function() {
          $ionicHistory.clearCache(['workflow.todo', 'workflow.trans']).then(function() {
            FlowUtils.goBackOrHome();
          });
        })
        .finally($ionicLoading.hide);
      }
    })
  }

  // 结束流程（自由流程）
  $scope.end = function() {
    IbPopup.confirm({ title: '确认要结束该工作流程吗？'})
    .then(function(res) {
      if(res) {
        $ionicLoading.show();
        Flow.signFlow($scope.key, {
          topflag: flow.rp.topflag,
          opflag: flow.rp.opflag
        })
        .success(function() {
          $ionicHistory.clearCache(['workflow.todo', 'workflow.trans']).then(function() {
            FlowUtils.goBackOrHome();
          });
        })
        .finally($ionicLoading.hide);
      }
    })
  }

  $scope.imageAttach = [];
  $scope.fileAttach = [];

  // 添加图片
  $scope.addImage = function(e) {
    var safeSize = 20;
    var files = e.target.files;
    if (FlowUpload.isSafeFile(FlowUpload.safeImgTypes, files) && FlowUpload.isSafeSize(safeSize, files)) {
      FlowUpload.upload($scope, files, $scope.imageAttach, $scope.flow.process.processid);
    } else {
      IbPopup.alert({
        title: '仅支持不超过'+safeSize+'M的以下类型文件：' + FlowUpload.safeImgTypes.join(',')
      });
    }
  }

  // 删除图片
  $scope.removeImage = function(index) {
    $scope.imageAttach.splice(index, 1);
  }

  // 添加文件
  $scope.isIos = ionic.Platform.isIOS();
  $scope.checkIos = function() {
    if ($scope.isIos) {
      return IbPopup.alert({
        title: '抱歉，暂不支持苹果系统及部分安卓系统上传附件'
      });
    }
  }

  $scope.addFile = function(e) {
    var files = e.target.files;
    var safeSize = 20;
    if (FlowUpload.hasEmptyFile(files)) {
      return IbPopup.alert({
        title: '上传的文件中不能包含空文件'
      });
    }
    if (FlowUpload.isSafeFile(FlowUpload.safeFileTypes, files) && FlowUpload.isSafeSize(safeSize, files)) {
      FlowUpload.upload($scope, files, $scope.fileAttach, $scope.flow.process.processid);
    } else {
      IbPopup.alert({
        title: '仅支持不超过'+safeSize+'M的以下类型文件：' + FlowUpload.safeFileTypes.join(',')
      });
    }
  }

  // 删除文件
  $scope.removeFile = function(index) {
    $scope.fileAttach.splice(index, 1);
  }

  // 下载文件
  $scope.downloadFile = function(item) {
    if (item.entireattachurl) {
      return FileUtils.download($filter('fullUrl')(item.entireattachurl));
    }
    if (item.url) {
      FileUtils.workflowDownload(item.url);
    }
  }

  // 将附件分成图片和文件
  function classifyAttach(attachData) {
    angular.forEach(attachData, function(attach) {
      var type = attach.filename.split('.').pop().toLowerCase();
      if (FlowUpload.safeImgTypes.indexOf(type) !== -1) {
        attach.url = $filter('fullUrl')(attach.officereadurl);
        $scope.imageAttach.push(attach);
      } else {
        $scope.fileAttach.push(attach);
      }
    });
  }

  // 对象转数组 
  function obj2Arr(obj) {
    var arr = [];
    angular.forEach(obj, function(value) {
      arr.push(value);
    });
    return arr;
  }

  $ionicLoading.show();

  Flow.getFlow($stateParams.key)
  .success(function(res) {
    if(typeof res.isSuccess !== 'undefined' && !res.isSuccess) {

      IbPopup.alert({ title: res.msg })
      .then(function() {
        $ionicHistory.goBack();
      });

    } else {
      flow = $scope.flow = res;
      // feedback没有数据的时候后端会返回空数组，有数据的时候会返回obj
      if ($scope.flow.feedback && !($scope.flow.feedback instanceof Array)) {
        $scope.flow.feedback = obj2Arr($scope.flow.feedback);
      }
      form.key = $scope.key;
      form.title = flow.run.name
      form.hidden = flow.hidden;
      form.readonly = flow.readonly;
      form.attachmentid = flow.run.attachmentid;
      form.fbattachmentid = ''; // ?
      form.topflag = flow.rp.topflag;
      form.saveflag = '';
      form.formhash = $scope.formhash;
      form.enablefiled = flow.enablefiled + '';
      $scope.ics = [].concat(flow.enableArr, flow.valueArr);
      // 分类附件(分成图片和文件)
      res.attachData && classifyAttach(res.attachData);
    }
  })
  .finally($ionicLoading.hide);
})

/**
 * @ngdoc controller
 * @name FlowHandoverCtrl
 * @module workflow.controllers
 * @description
 *   转交工作页控制器
 */
.controller('FlowHandoverCtrl', function($scope, $state, $stateParams, $timeout, $ionicLoading, IbPopup, Flow, FlowUtils) {
  var data = $scope.data = {};
  var form = $scope.form = {
    'prcs_user_op': [],
    'prcs_user': [],
    'topflag': [],
    'prcs_choose': {},
    'remind': {
      '1': true,
      '2': false,
      '3': false
    }
  };

  $scope.save = function() {

    // 转交前有未经办人员时确认
    if(data.notAllFinished) {
      var msg = '经办人 ' + data.notAllFinished + ' 未办理完毕';

      // 
      // 没要所有经办人会签时, 提示确认。
      if(data.process.turnpriv == '1') {
        if(!window.confirm(msg + ', 是否确认转交？')) {
          return false;
        }
      // 要求所有经办人会签时，给出消息提示，但不允许转交
      } else if(data.process.turnpriv == '0') {
        alert(msg);
        return false;
      }
    }
    
    var formData = {
      'runid': data.runid,
      'flowid': data.flowid,
      'processid': data.processid,
      'flowprocess': data.flowprocess,
      'processto': data.process.processto,
      'prcsback': data.prcsback,

      'message': form.message,
      'remind': {},
      'prcs_choose': []
    };

    // 格式化 scope 的数据以提交给服务器
    angular.forEach(form.prcs_choose, function(v, i) {
      if(v.checked) {
        formData['prcs_user_op' + i] = form.prcs_user_op[i] || '';
        formData['prcs_user' + i] = form.prcs_user[i] || '';
        formData['topflag' + i] = form.topflag[i] || '0';

        // 若非最后一步骤时，需要通过 prcs_choose 储存选中步骤信息
        // 最后一步则保持 prcs_choose 为空保持后端判断的标准
        // 给这逻辑跪下了...
        if(!v.isover) {
          formData.prcs_choose.push(v.value);
        }
      }
    });

    // 由于后端使用 isset 判断 remind，所有要清理 remind 值为 false 的 key
    // 不然就算值为 false 后端也会判断为 true
    angular.forEach(form.remind, function(v, i){
      if(v) {
        formData.remind[i] = v;
      }
    });

    // 如果是最后一步，则需要用特殊的参数名...
    // @ps. 在流程本身为子流程的情况下，就算是最后一步，也会有主办人和经办人选项
    if(!formData.prcs_choose.length) {
      formData.prcs_user_op = formData.prcs_user_op0;
      formData.prcs_user = formData.prcs_user0;
      delete formData.prcs_user_op0;
      delete formData.prcs_user0;
      delete formData.topflag0;
    }
    formData.topflag = $stateParams.topflag;
    $ionicLoading.show();
    Flow.handover(formData)
    .success(FlowUtils.onHandoverSuccess)
    .finally($ionicLoading.hide);
  }

  // 初始化步骤信息
  $scope.initStep = function(index, step) {
    form.prcs_choose[index] = {
      checked: step.checked,
      value: index,
      isover: step.isover
    }
    if(index === 0 && step.isover === true){
        $scope.selectStep(index, step);
    }
  };

  // 点击其他步骤时，判断是否允许并发
  // 允许时，判定为多选
  // 不允许时，判定为切换步骤（仿单选功能）
  $scope.selectStep = function(index, step) {
    // 结束流程的提醒是特殊的默认选择
    if(step.isover == true) {
        form.remind[1] = false;
        form.remind[2] = true;
        $scope.form.message = '【' + $scope.data.run.name + '】流程已办理完毕，请在“流程结束”分类下查看该流程。谢谢。';
    } else {
        form.remind[1] = true;
        form.remind[2] = false;
        $scope.form.message = '办理工作【' + $scope.data.run.name + '】，谢谢';
    }
    if($scope.data.process.syncdeal != 0) {
      return false;
    }
    if(form.prcs_choose[index].checked) {
      angular.forEach(form.prcs_choose, function(s) {
        s.checked = false;
      });
    }
    form.prcs_choose[index].checked = true;
  };

  // 判断指定步骤是否子流程
  $scope.notChildFlow = function(step) {
    return step.process && 
      step.process.childflow &&
      step.process.childflow == '0'
  };

  // 验证转交表单
  $scope.validate = function() {
    var ret = true;  
    var prcs = form.prcs_choose;
    
    // 针对这种按顺序验证的情况, 先抽离所有已选的进行判断
    var checked = []
    for(var key in prcs){
      if (prcs.hasOwnProperty(key) && prcs[key].checked) {
        checked.push(key)
      }
    }

    for (var i = 0; i < checked.length; i++) {
      var key = checked[i]
      if (prcs[key].isover) {
        ret = true;
        // 为子流程的情况
        if (data.prcsback) {
          ret = !!form.prcs_user_op[key];
          if(!ret) break
        }
      } else {
        // 当主办人选项为 ‘主办’时，要求主办人必填
        if (form.topflag[key] == '0') {
          ret = !!form.prcs_user_op[key];
        } else {
          // 否则主办人选项为 其他时，要求经办人必填
          ret = !!form.prcs_user[key];
        }
        if (!ret) break
      }
    }

    return ret;
  };

  $ionicLoading.show();

  Flow.getNext($stateParams)
  .success(function(res) {
    if(typeof res == 'string') {
      IbPopup.alert({ title: res})
    } else {
      data = $scope.data = res;
      $scope.form.message = '请您及时办理工作【' + $scope.data.run.name + '】，谢谢';
    }
  })
  .finally($ionicLoading.hide);
})

/**
 * @ngdoc controller
 * @name FlowHandoverFreeCtrl
 * @module workflow.controllers
 * @description
 *   转交自由流程页控制器
 */
.controller('FlowHandoverFreeCtrl', function($scope, $state, $stateParams, $timeout, $ionicLoading, $ionicModal, $ionicScrollDelegate, IbPopup, User, Flow, FlowUtils) {
  var run = $scope.run = {};
  var form = $scope.form = {
    'next': {
      'topflag': '0'
    },
    'preset': [],
    'field': []
  };

  // 增加预设步骤
  $scope.addPreset = function() {
    form.preset.push({
      'topflag': '0',
      'prcsUserOp': '',
      'prcsUser': ''
    })

    $timeout(function() {
      $ionicScrollDelegate.resize()
    })
  };

  // 创建分类详细 Modal
  $ionicModal.fromTemplateUrl('./templates/workflow/workflow-field-modal.html', {
    scope: $scope,
    animation: 'slide-in-up',
  })
  .then(function(modal) {
    $scope.modal = modal;
  })

  // 移除预设步骤
  $scope.removePreset = function($index) {
    form.preset.splice($index, 1);
  };

  $scope.getPresetIndex = function($index, processid) {
    return $index + 1 + +run.processid;
  }

  $scope.save = function() {
    // 如果还有经办人未处理，则弹出提示
    if(run.notAllFinished) {
      var msg = '经办人 ' + run.notAllFinished + ' 未办理完毕';
      if(!window.confirm(msg + ', 是否确认转交？')) {
        return false;
      }
    }
    
    var formData = {
      'op': run.op,
      'key': run.key,
      'topflagOld': run.topflag,
      'freeItemOld': '',
      'tmpCount': '',
      'lineCount': form.preset.length,
      'message': form.message,
      'formhash': User.formhash
    };

    // 若下一步骤为预设步骤
    if(run.preset != '') {
      formData.preset = '1';
      formData.prcsUserOp = '1';
      formData.prcsUser = '1';
      formData.topflag = '1';
    // 否则手动指定下一步骤
    } else {

      // 拿到下一步骤的数据
      formData.topflag = form.next.topflag;
      formData.prcsUserOp = User.addPrefix(form.next.prcsUserOp);
      formData.prcsUser = User.addPrefix(form.next.prcsUser);

      // 拿到预设步骤的数据
      angular.forEach(form.preset, function(preset, index) {
        var flag = index + 1;
        formData['topflag' + flag] = preset.topflag;
        formData['prcsUserOp' + flag] = User.addPrefix(preset.prcsUserOp);
        formData['prcsUser' + flag] = User.addPrefix(preset.prcsUser);
      });
  
      // 拿到可写字段数据
      angular.forEach(form.field, function(f) {
        formData.freeItemOld += (formData.freeItemOld ?  ',' + f : f);
      });
    }

    $ionicLoading.show();
    Flow.handoverFree(formData)
    .success(FlowUtils.onHandoverSuccess)
    .finally($ionicLoading.hide);
  }
  
  function valiStep (step) {
    // 下一步骤主办人选项为“主办”，则要求主办人非空
    if(step.topflag == '0' && !step.prcsUserOp) {
      return false;
    }

    // 否则要求经办人非空
    if(step.topflag != '0' && !step.prcsUser) {
      return false;
    }

    return true;
  }

  // 验证转交表单
  $scope.validate = function() {
    // 如果是预设步骤可直接提交
    if(run.preset != '') {
      return true;
    }

    // 下一步骤主办人选项为“主办”，则要求主办人非空
    if(!valiStep(form.next)) {
      return false;
    }

    // 有预设步骤时，判断预设步骤数据是否合理，逻辑同上
    var i = 0, len;
    if(len = form.preset.length) {
      for(; i < len; i++) {
        if(!valiStep(form.preset[i])) {
          return false;
        }
      }
    }

    return true;
  };

  $ionicLoading.show();
  Flow.getFreeNext($stateParams.key)
  .success(function(res) {
    if(typeof res == 'string') {
      IbPopup.alert({ title: res})
    } else {
      run = $scope.run = res;
      $scope.form.message = '请您及时办理工作【' + run.runName + '】，谢谢';
    }
  })
  .finally($ionicLoading.hide);

  $scope.$on('$ionicView.leave', function() {
    $scope.modal.hide();
  });
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
})

/**
 * @ngdoc controller
 * @name FlowDetailCtrl
 * @module workflow.controllers
 * @description
 *   查看工作页控制器
 */
.controller('FlowDetailCtrl', function($scope, $state, $stateParams, $timeout, $window, $ionicLoading, $ionicPopup, $filter, IbPopup, Settings, User, Flow, FlowUtils, FileUtils, FlowUpload) {

  $scope.flow = {};
  $scope.key = $stateParams.key;
  $scope.formhash = User.formhash;

  // 查看原表
  $scope.viewSource = function() {
    $window.open(Settings.hostUrl + '?r=workflow/preview/print&key=' + $scope.key, '_blank', 'location=yes');
  }

  $ionicLoading.show();

  $scope.imageAttach = [];
  $scope.fileAttach = [];

  // 将附件分成图片和文件
  function classifyAttach(attachData) {
    angular.forEach(attachData, function(attach) {
      var type = attach.filename.split('.').pop().toLowerCase();
      if (FlowUpload.safeImgTypes.indexOf(type) !== -1) {
        attach.url = $filter('fullUrl')(attach.officereadurl);
        $scope.imageAttach.push(attach);
      } else {
        $scope.fileAttach.push(attach);
      }
    });
  }

  // 对象转数组 
  function obj2Arr(obj) {
    var arr = [];
    angular.forEach(obj, function(value) {
      arr.push(value);
    });
    return arr;
  }

  $scope.downloadFile = function(item) {
    if (item.entireattachurl) {
      return FileUtils.download($filter('fullUrl')(item.entireattachurl));
    }
    if (item.url) {
      FileUtils.workflowDownload(item.url);
    }
  }

  Flow.getFlow($stateParams.key, { type: 'view' })
  .success(function(res) {
    if(typeof res.isSuccess !== 'undefined' && !res.isSuccess) {
      IbPopup.alert({ title: '提示', template: res.msg })
      .then(FlowUtils.goBackOrHome)
    } else {
      $scope.flow = res;
      // feedback没有数据的时候后端会返回空数组，有数据的时候会返回obj
      if ($scope.flow.feedback && !($scope.flow.feedback instanceof Array)) {
        $scope.flow.feedback = obj2Arr($scope.flow.feedback);
      }
      res.attachData && classifyAttach(res.attachData);
    }
  })
  .finally($ionicLoading.hide);
})

