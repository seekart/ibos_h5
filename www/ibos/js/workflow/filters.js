/**
 * @ngdoc overview
 * @name  workflow.filters
 * @description
 *   工作流模块过滤器
 */

angular.module('workflow.filters', [])

/**
 * @ngdoc filter
 * @name workflow.filters.filter:wfStatus
 * @function
 * @description
 *  将工作流进行状态标识转化为对应中文
 *
 * @param   {string}  val   控件原始值
 * @param   {object}  item  控件源数据
 * @returns {string}  修正后的控件值
 */

.filter('wfStatus', function() {
  var flagMap = {
    1: '未接收',
    2: '办理中',
    3: '转交下一步，下一步经办人无人接收',
    5: '自由流程预设步骤',
    4: '已办结',
    6: '已挂起'
  };

  return function(flag) {
    return flagMap[+flag] || '';
  };
})

/**
 * @ngdoc filter
 * @name workflow.filters.filter:wfic
 * @function
 * @description
 *  修正表单控件的显示值
 *
 * @param   {string}  val   控件原始值
 * @param   {object}  item  控件源数据
 * @returns {string}  修正后的控件值
 */
.filter('wfic', function(userInfoFilter, departmentInfoFilter, positionInfoFilter, User) {

  var filters = {
    'user': function(item) {
      var val = item['origin-value']
      switch(item['data-select-type']) {
        case 'department':
          return departmentInfoFilter(User.removeDepartmentPrefix(val), 'deptname');

        case 'position':
          return positionInfoFilter(User.removePositionPrefix(val), 'posname');

        default:
          return userInfoFilter(User.removePrefix(val), 'realname');
      }
    }
  };

  return function(item) {
    var dataType = item['data-type']
    return filters[dataType] ? 
      filters[dataType].call(null, item) :
      item['origin-value'];
  }
})

/**
 * @ngdoc filter
 * @name workflow.filters.filter:attrFromTemplate
 * @function
 * @description
 *  从 html 元素模板中获取属性值
 *
 * @param   {object}  flowItem     控件源数据
 * @param   {string}  checkItem   原始验证规则
 * @returns {string}  转换后的验证规则
 */
.filter('attrFromTemplate', function() {

  return function(template, attr) {
    var $elem;
    if(template) {
      $elem = angular.element(template);
      if($elem.length && attr) {
        return $elem.attr(attr) || '';
      }
    }
    return '';
  }
})

/**
 * @ngdoc filter
 * @name workflow.filters.filter:icPattern
 * @function
 * @description
 *  返回表单控件的规则类型
 *
 * @param   {string}  icTitle     控件名称
 * @param   {string}  checkItem   原始验证规则
 * @returns {string}  规则类型
 */
.filter('icPatternType', function() {
  // 验证字段为如“字段1=type1,字段2=type2”这种格式的字符串
  // 此方法用于将字符串解析成 key-value 的结构
  function getCheckItemMap(str) {
    var ret = {};

    str.split(',').forEach(function(exp) {
      var parts = exp.split('=');
      ret[parts[0]] = parts[1];
    });

    return ret;
  }

  return function(icTitle, checkItem) {
    var type = '';
    var checkItemMap;

    if(icTitle && checkItem) {
      checkItemMap = getCheckItemMap(checkItem);
      type = checkItemMap[icTitle] || ''
    }

    return type;
  }
})

.filter('icPatternByType', function(icPatternTypeFilter, REGEXP_ENUM) {

  return function(type) {
    return REGEXP_ENUM[type] || '';
  };
})

/**
 * @ngdoc filter
 * @name workflow.filters.filter:icPattern
 * @function
 * @description
 *  返回表单控件的具体验证规则
 *
 * @param   {object}  icTitle     控件名称
 * @param   {string}  checkItem   原始验证规则
 * @returns {string}  验证规则，Regex 字符串
 */
.filter('icPattern', function(icPatternTypeFilter, REGEXP_ENUM) {

  return function(icTitle, checkItem) {
    return REGEXP_ENUM[icPatternTypeFilter(icTitle, checkItem)] || '';
  };
})

/**
 * @ngdoc filter
 * @name workflow.filters.filter:wfFilenameFormat
 * @function
 * @description
 *  上传文件文件名显示格式为 【前14个字】...【后4个字】.【后缀名】
 * @param  {string}  str  文件名
 * @return {string}  处理后的文件名
 */
.filter('wfFilenameFormat', function() {
  // 解析html实体编号
  var htmlDecode = function(str) {
    return str.replace(/&#(x)?([^&]{1,5});?/g,function($,$1,$2) {
      return String.fromCharCode(parseInt($2 , $1 ? 16:10));
    });
  }
  return function(str) {
    // 文件名后缀
    var strArr = htmlDecode(str).split('.');
    var ext = '.' + strArr.pop();
    var filenameWithoutExt = strArr.join('.');
    if (filenameWithoutExt.length > 18) {
      return filenameWithoutExt.slice(0, 14) + '...' + filenameWithoutExt.slice(-4) + ext;
    }
    return str;
  }
})

/**
 * @ngdoc filter
 * @name workflow.filters.filter:wfIsSponsor
 * @function
 * @description
 *  当前用户是否为流程主办人
 * @param  {object}  flow  流程数据
 * @return {boolean} 是否
 */
.filter('wfIsSponsor', function() {
  return function(flow) {
    return flow && flow.rp && flow.rp.opflag == '1'
  }
})

/**
 * @ngdoc filter
 * @name workflow.filters.filter:sumArrayBy
 * @function
 * @description
 *   合计多个数组里指定 index 的值
 *
 * @example
 *   {{ [[1, 2], [2, 3]] | sumArrayBy }} => 3
 *   {{ [[1, 2], [2, 3]] | sumArrayBy:1 }} => 5
 * @param  {array}   arrs       数组
 * @param  {number}  [index=0]  下标
 */
.filter('sumArrayBy', function() {
  return function(arrs, index) {
    index = angular.isDefined(index) ? index : 0

    var indexArr = arrs.map(function(arr) {
      if(angular.isArray(arr) && angular.isDefined(arr)) {
        return arr[index]
      } else {
        return 0
      }
    })

    return indexArr.reduce(function(a, b) {
      a = isNaN(+a) ? 0 : +a
      b = isNaN(+b) ? 0 : +b
      return a + b
    }, 0)
  }
})