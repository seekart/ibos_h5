/**
 * @ngdoc module
 * @name workflow
 * @description
 *   工作流模块
 */

angular.module('workflow', ['ionic', 'workflow.controllers', 'workflow.services', 'workflow.filters', 'workflow.directives'])

.run(function(Apps) {
  Apps.install({ name: 'workflow', title: '工作流', route: '#/workflow', state: 'workflow.index', icon: 'img/modicon/workflow.png' })
})

.config(function($stateProvider, $urlRouterProvider, UserProvider, ResolversProvider) {

  $urlRouterProvider.when('/workflow', '/workflow/index');

  $stateProvider
  .state('workflow', {
    url: '/workflow',
    abstract: true,
    template: '<ion-nav-view name="workflow"></ion-nav-view>',
    resolve: UserProvider.resolve
  })

  $stateProvider
  // 工作流-首页
  .state('workflow.index', {
    url: '/index',
    talkingDataLabel: '工作流-首页',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-index.html',
        controller:'FlowIndexCtrl'
      }
    }
  })

  // 工作流-待办工作
  .state('workflow.todo', {
    url: '/todo',
    talkingDataLabel: '工作流-待办工作',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-list.html',
        controller: 'FlowTodoCtrl'
      }
    }
  })


  // 工作流-已办结（已转交）
  .state('workflow.trans', {
    url: '/trans',
    talkingDataLabel: '工作流-已办结',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-list.html',
        controller: 'FlowTransCtrl'
      }
    }
  })

  // 工作流-已完成
  .state('workflow.done', {
    url: '/done',
    talkingDataLabel: '工作流-已完成',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-list.html',
        controller: 'FlowDoneCtrl'
      }
    }
  })

  // 工作流-我的关注
  .state('workflow.follow', {
    url: '/follow',
    talkingDataLabel: '工作流-我的关注',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-follow-list.html',
        controller: 'FlowFollowCtrl'
      }
    }
  })

  // 工作流-新建
  .state('workflow.create', {
    url: '/create',
    talkingDataLabel: '工作流-发布工作',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-create.html',
        controller: 'FlowCreateCtrl'
      }
    }
  })

  // 工作流-主办
  .state('workflow.handle', {
    url: '/handle/:key?topflag',
    talkingDataLabel: '工作流-主办',
    resolve: ResolversProvider.get(['canvasPainter']),
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-handle.html',
        controller: 'FlowHandleCtrl'
      }
    }
  })
 
  .state('workflow.handover', {
    url: '/handover/:key?topflag',
    talkingDataLabel: '工作流-转交',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-handover.html',
        controller: 'FlowHandoverCtrl'
      }
    }
  })

  .state('workflow.handoverFree', {
    url: '/handoverFree/:key',
    talkingDataLabel: '工作流-转交自由流程',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-handover-free.html',
        controller: 'FlowHandoverFreeCtrl'
      }
    }
  })

  // 工作流-详细
  .state('workflow.detail', {
    url: '/detail/:key',
    talkingDataLabel: '工作流-查看工作流',
    views: {
      'workflow': {
        templateUrl: 'templates/workflow/workflow-detail.html',
        controller: 'FlowDetailCtrl'
      }
    }
  })
  
})

.run(function($rootScope, $ionicHistory) {

  // function clearListCache() {
  //   $ionicHistory.clearCache(['workflow.todo', 'workflow.trans', 'workflow.done']);
  // }

  // angular.forEach([
  //   'workflow.handover',
  //   'workflow.handoverFree',
  //   'workflow.signFlow',
  //   'workflow.takeback',
  // ], function(eventName) {
  //   $rootScope.$on(eventName, clearListCache);
  // });
})